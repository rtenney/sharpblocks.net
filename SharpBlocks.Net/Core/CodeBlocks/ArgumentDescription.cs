﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace SharpBlocks.Net.Core.CodeBlocks
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [DesignerCategory(@"code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot("Arguments-description", Namespace = "", IsNullable = false)]
    public class ArgumentDescription
    {
        private string _n;

        private string _name;

        private string _value;

        [XmlAttribute("n")]
        public string N
        {
            get { return _n; }
            set
            {
                if ((_n != null))
                {
                    if ((_n.Equals(value) != true))
                    {
                        _n = value;
                    }
                }
                else
                {
                    _n = value;
                }
            }
        }

        [XmlAttribute("name")]
        public string Name
        {
            get { return _name; }
            set
            {
                if ((_name != null))
                {
                    if ((_name.Equals(value) != true))
                    {
                        _name = value;
                    }
                }
                else
                {
                    _name = value;
                }
            }
        }

        [XmlText]
        public string Value
        {
            get { return _value; }
            set
            {
                if ((value != null))
                {
                    if ((value.Equals(_value) != true))
                    {
                        _value = value;
                    }
                }
                else
                {
                    _value = value;
                }
            }
        }
    }
}