﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace SharpBlocks.Net.Core.CodeBlocks
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [DesignerCategory(@"code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class BlockConnectors //: EntityBase<BlockConnectors>
    {
        
        private List<BlockConnector> _blockConnector;

        [XmlElement("BlockConnector", Order = 0)]
        public List<BlockConnector> BlockConnector
        {
            get
            {
                if ((_blockConnector == null))
                {
                    _blockConnector = new List<BlockConnector>();
                }
                return _blockConnector;
            }
            set
            {
                if ((_blockConnector != null))
                {
                    if ((_blockConnector.Equals(value) != true))
                    {
                        _blockConnector = value;
                        //OnPropertyChanged("BlockConnector");
                    }
                }
                else
                {
                    _blockConnector = value;
                    //OnPropertyChanged("BlockConnector");
                }
            }
        }
    }
}