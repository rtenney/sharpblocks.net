﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;
using SharpBlocks.Net.Core.Utilities;

namespace SharpBlocks.Net.Core.CodeBlocks
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [DesignerCategory(@"code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot("BlockImageIcon", Namespace = "", IsNullable = false)]
    public class BlockImageIcon //: EntityBase<BlockImageIcon>
    {
        private ImageLocation _blockLocation;
        private string _fileLocation;
        private int _height;

        private int _width;

        public BlockImageIcon()
        {
            IsWrapTextEnabled = false;
            IsImageEditable = false;
            _blockLocation = ImageLocation.center;
        }

        [XmlAttribute("block-location")]
        [DefaultValue(ImageLocation.center)]
        public ImageLocation BlockLocation
        {
            get
            {
                return _blockLocation;
            }
            set
            {
                if ((_blockLocation.Equals(value) != true))
                {
                    _blockLocation = value;

                    //OnPropertyChanged("BlockLocation");
                }
            }
        }

        [XmlElement(Order = 0)]
        public string FileLocation
        {
            get
            {
                return _fileLocation;
            }
            set
            {
                if ((_fileLocation != null))
                {
                    if ((_fileLocation.Equals(value) != true))
                    {
                        _fileLocation = value;

                        //OnPropertyChanged("FileLocation");
                    }
                }
                else
                {
                    _fileLocation = value;

                    //OnPropertyChanged("FileLocation");
                }
            }
        }

        [XmlAttribute("height")]
        public int Height
        {
            get
            {
                return _height;
            }
            set
            {
                if ((_height != null))
                {
                    if ((_height.Equals(value) != true))
                    {
                        _height = value;

                        //OnPropertyChanged("Height");
                    }
                }
                else
                {
                    _height = value;

                    //OnPropertyChanged("Height");
                }
            }
        }

        [XmlIgnore]
        public bool IsImageEditable { get; set; }

        [XmlAttribute("image-editable")]
        [DefaultValue("no")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string IsImageEditableString
        {
            get
            {
                return XmlTypeConverter.ConvertFromBoolean(IsImageEditable);
            }
            set
            {
                if ((IsImageEditable.Equals(value) != true))
                {
                    IsImageEditable = XmlTypeConverter.ConvertToBoolean(value);

                    //OnPropertyChanged("IsImageEditableString");
                }
            }
        }

        [XmlIgnore]
        public bool IsWrapTextEnabled { get; set; }

        [XmlAttribute("width")]
        public int Width
        {
            get
            {
                return _width;
            }
            set
            {
                if ((_width != null))
                {
                    if ((_width.Equals(value) != true))
                    {
                        _width = value;

                        //OnPropertyChanged("Width");
                    }
                }
                else
                {
                    _width = value;

                    //OnPropertyChanged("Width");
                }
            }
        }

        [XmlAttribute("wrap-text")]
        [DefaultValue("no")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string WrapTextString
        {
            get
            {
                return XmlTypeConverter.ConvertFromBoolean(IsWrapTextEnabled);
            }
            set
            {
                if (IsWrapTextEnabled.Equals(value)) return;
                IsWrapTextEnabled = XmlTypeConverter.ConvertToBoolean(value);

                //OnPropertyChanged("WrapTextString");
            }
        }
    }
}