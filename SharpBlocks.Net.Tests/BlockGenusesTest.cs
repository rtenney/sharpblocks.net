﻿using SharpBlocks.Net.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using SharpBlocks.Net.Core.CodeBlocks;

namespace SharpBlocks.Net.Tests
{
    
    
    /// <summary>
    ///This is a test class for BlockGenusesTest and is intended
    ///to contain all BlockGenusesTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BlockGenusesTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for BlockGenuses Constructor
        ///</summary>
        [TestMethod()]
        public void BlockGenusesConstructorTest()
        {
            BlockGenuses target = new BlockGenuses();
            Assert.IsNotNull(target);
            //Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for BlockGenus
        ///</summary>
        [TestMethod()]
        public void BlockGenusTest()
        {
            BlockGenuses target = new BlockGenuses(); // TODO: Initialize to an appropriate value
            List<BlockGenus> expected = new List<BlockGenus>(); // TODO: Initialize to an appropriate value
            List<BlockGenus> actual;
            target.BlockGenus = expected;
            actual = target.BlockGenus;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
