﻿using SharpBlocks.Net.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SharpBlocks.Net.Core.Utilities;

namespace SharpBlocks.Net.Tests
{
    
    
    /// <summary>
    ///This is a test class for XmlTypeConverterTest and is intended
    ///to contain all XmlTypeConverterTest Unit Tests
    ///</summary>
    [TestClass()]
    public class XmlTypeConverterTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for ConvertFromBoolean
        ///</summary>
        [TestMethod()]
        public void ConvertFromBooleanTest()
        {
            bool value = false; // TODO: Initialize to an appropriate value
            string expected = "no"; // TODO: Initialize to an appropriate value
            string actual;
            actual = XmlTypeConverter.ConvertFromBoolean(value);
            Assert.AreEqual(expected, actual);

            value = true; // TODO: Initialize to an appropriate value
            expected = "yes"; // TODO: Initialize to an appropriate value
            actual = XmlTypeConverter.ConvertFromBoolean(value);
            Assert.AreEqual(expected, actual);

            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ConvertToBoolean
        ///</summary>
        [TestMethod()]
        public void ConvertToBooleanTest()
        {
            string value = "no"; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = XmlTypeConverter.ConvertToBoolean(value);
            Assert.AreEqual(expected, actual);

            value = "yes"; // TODO: Initialize to an appropriate value
            expected = true; // TODO: Initialize to an appropriate value
            actual = XmlTypeConverter.ConvertToBoolean(value);
            Assert.AreEqual(expected, actual);

            //Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
