﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace SharpBlocks.Net.Core.CodeBlocks
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [DesignerCategory(@"code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class BlockConnectorShape //: EntityBase<BlockConnectorShape>
    {
        private int _shapenumber;
        private string _shapetype;

        [XmlAttribute("shape-type")]
        public string ShapeType
        {
            get { return _shapetype; }
            set
            {
                if ((_shapetype != null))
                {
                    if ((_shapetype.Equals(value) != true))
                    {
                        _shapetype = value;
                        //OnPropertyChanged("ShapeType");
                    }
                }
                else
                {
                    _shapetype = value;
                    //OnPropertyChanged("ShapeType");
                }
            }
        }

        [XmlAttribute("shape-number")]
        public int ShapeNumber
        {
            get { return _shapenumber; }
            set
            {
                if (_shapenumber.Equals(value)) return;
                _shapenumber = value;
                //OnPropertyChanged("ShapeNumber");
            }
        }
    }
}