﻿using System;
using SharpBlocks.Net.Model;

namespace SharpBlocks.Net.Model
{
    public class DataService : IDataService
    {
        public void GetData(Action<DataItem, Exception> callback)
        {
            // <pex>
            if (callback == (Action<DataItem, Exception>)null)
                throw new ArgumentNullException("callback");
            // </pex>
            // Use this to connect to the actual data service

            var item = new DataItem("Welcome to MVVM Light");
            callback(item, null);
        }
    }
}