// <copyright file="EntityBaseTTest.cs">Copyright �  2013</copyright>
namespace SharpBlocks.Net.Core
{
    /// <summary>This class contains parameterized unit tests for EntityBase`1</summary>
    [global::Microsoft.Pex.Framework.PexClass(typeof(global::SharpBlocks.Net.Core.EntityBase<>))]
    [global::Microsoft.Pex.Framework.Validation.PexAllowedExceptionFromTypeUnderTest(typeof(global::System.InvalidOperationException))]
    [global::Microsoft.Pex.Framework.Validation.PexAllowedExceptionFromTypeUnderTest(typeof(global::System.ArgumentException), AcceptExceptionSubtypes = true)]
    [global::Microsoft.VisualStudio.TestTools.UnitTesting.TestClass]
    public partial class EntityBaseTTest
    {
        /// <summary>Test stub for Clone()</summary>
        [global::Microsoft.Pex.Framework.PexGenericArguments(typeof(int))]
        [global::Microsoft.Pex.Framework.PexMethod]
        public T Clone<T>([global::Microsoft.Pex.Framework.PexAssumeUnderTest]global::SharpBlocks.Net.Core.EntityBase<T> target)
        {
            T result = target.Clone();
            return result;
            // TODO: add assertions to method EntityBaseTTest.Clone(EntityBase`1<!!0>)
        }

        /// <summary>Test stub for .ctor()</summary>
        [global::Microsoft.Pex.Framework.PexGenericArguments(typeof(int))]
        [global::Microsoft.Pex.Framework.PexMethod]
        public global::SharpBlocks.Net.Core.EntityBase<T> Constructor<T>()
        {
            global::SharpBlocks.Net.Core.EntityBase<T> target
               = new global::SharpBlocks.Net.Core.EntityBase<T>();
            return target;
            // TODO: add assertions to method EntityBaseTTest.Constructor()
        }

        /// <summary>Test stub for OnPropertyChanged(String)</summary>
        [global::Microsoft.Pex.Framework.PexGenericArguments(typeof(int))]
        [global::Microsoft.Pex.Framework.PexMethod]
        public void OnPropertyChanged<T>(
            [global::Microsoft.Pex.Framework.PexAssumeUnderTest]global::SharpBlocks.Net.Core.EntityBase<T> target,
            string propertyName
        )
        {
            target.OnPropertyChanged(propertyName);
            // TODO: add assertions to method EntityBaseTTest.OnPropertyChanged(EntityBase`1<!!0>, String)
        }
    }
}
