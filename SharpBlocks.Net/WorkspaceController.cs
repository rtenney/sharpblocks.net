﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Threading;
using SharpBlocks.Net.Core;
using System.IO;
using SharpBlocks.Net.Core.Workspace;
using SharpBlocks.Net.Core.CodeBlocks;
using SharpBlocks.Net.Core.Rules;
using SharpBlocks.Net.Core.Utilities;
using GalaSoft.MvvmLight;

namespace SharpBlocks.Net
{
    public class WorkspaceController : ViewModelBase
    {
        private static string LANG_DEF_FILEPATH;
        private Workspace _workspace;
        private bool _langDefDirty;
        private bool _workspaceLoaded;
        private BlockLanguageDefinition _landDefRoot;

        private WorkspaceController()
        {
            _workspace = Workspace.GetInstance();
        }

        private static void RunSharpBlocks()
        {
            LANG_DEF_FILEPATH = @"lang_def.xml";
            WorkspaceController wc = new WorkspaceController();

            wc._langDefDirty = true;
            wc.LoadFreshWorkspace();
            CreateAndShowGui(wc);
        }

        private static void CreateAndShowGui(WorkspaceController wc)
        {
            throw new NotImplementedException();
        }

        private void LoadFreshWorkspace()
        {
            if (_workspaceLoaded)
                ResetWorkspace();

            if (_langDefDirty)
                LoadBlockLanguage();

            _workspace.LoadWorkspaceFrom(null, _landDefRoot);

            _workspaceLoaded = true;
        }

        private void LoadBlockLanguage()
        {
            _landDefRoot = File.ReadAllText(@"lang_def.xml").Deserialize<BlockLanguageDefinition>();

            BlockLinkChecker.AddRule(new CommandRule());
            BlockLinkChecker.AddRule(new SocketRule());

            _langDefDirty = false;
        }

        private void ResetWorkspace()
        {
            _workspace.Reset();
        }

        public static void BeginController()
        {
            Dispatcher.CurrentDispatcher.BeginInvoke(new Action(() => RunSharpBlocks()));
        }
    }
}
