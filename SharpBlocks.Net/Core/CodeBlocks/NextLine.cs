﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SharpBlocks.Net.Core.CodeBlocks
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [DesignerCategory(@"code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class NextLine //: EntityBase<NextLine>
    {
    }
}