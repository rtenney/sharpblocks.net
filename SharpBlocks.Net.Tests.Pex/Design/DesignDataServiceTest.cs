// <copyright file="DesignDataServiceTest.cs">Copyright �  2013</copyright>
namespace SharpBlocks.Net.Design
{
    /// <summary>This class contains parameterized unit tests for DesignDataService</summary>
    [global::Microsoft.Pex.Framework.PexClass(typeof(global::SharpBlocks.Net.Design.DesignDataService))]
    [global::Microsoft.Pex.Framework.Validation.PexAllowedExceptionFromTypeUnderTest(typeof(global::System.InvalidOperationException))]
    [global::Microsoft.Pex.Framework.Validation.PexAllowedExceptionFromTypeUnderTest(typeof(global::System.ArgumentException), AcceptExceptionSubtypes = true)]
    [global::Microsoft.VisualStudio.TestTools.UnitTesting.TestClass]
    public partial class DesignDataServiceTest
    {
        /// <summary>Test stub for GetData(Action`2&lt;DataItem,Exception&gt;)</summary>
        [global::Microsoft.Pex.Framework.PexMethod]
        public void GetData(
            [global::Microsoft.Pex.Framework.PexAssumeUnderTest]global::SharpBlocks.Net.Design.DesignDataService target,
            global::System.Action<global::SharpBlocks.Net.Model.DataItem, global::System.Exception> callback
        )
        {
            target.GetData(callback);
            // TODO: add assertions to method DesignDataServiceTest.GetData(DesignDataService, Action`2<DataItem,Exception>)
        }
    }
}
