// <copyright file="MainViewModelTest.cs">Copyright �  2013</copyright>
namespace SharpBlocks.Net.ViewModel
{
    /// <summary>This class contains parameterized unit tests for MainViewModel</summary>
    [global::Microsoft.Pex.Framework.PexClass(typeof(global::SharpBlocks.Net.ViewModel.MainViewModel))]
    [global::Microsoft.Pex.Framework.Validation.PexAllowedExceptionFromTypeUnderTest(typeof(global::System.InvalidOperationException))]
    [global::Microsoft.Pex.Framework.Validation.PexAllowedExceptionFromTypeUnderTest(typeof(global::System.ArgumentException), AcceptExceptionSubtypes = true)]
    [global::Microsoft.VisualStudio.TestTools.UnitTesting.TestClass]
    public partial class MainViewModelTest
    {
        /// <summary>Test stub for .ctor(IDataService)</summary>
        [global::Microsoft.Pex.Framework.PexMethod]
        public global::SharpBlocks.Net.ViewModel.MainViewModel Constructor(global::SharpBlocks.Net.Model.IDataService dataService)
        {
            global::SharpBlocks.Net.ViewModel.MainViewModel target
               = new global::SharpBlocks.Net.ViewModel.MainViewModel(dataService);
            return target;
            // TODO: add assertions to method MainViewModelTest.Constructor(IDataService)
        }

        /// <summary>Test stub for get_WelcomeTitle()</summary>
        [global::Microsoft.Pex.Framework.PexMethod]
        public string WelcomeTitleGet([global::Microsoft.Pex.Framework.PexAssumeUnderTest]global::SharpBlocks.Net.ViewModel.MainViewModel target)
        {
            string result = target.WelcomeTitle;
            return result;
            // TODO: add assertions to method MainViewModelTest.WelcomeTitleGet(MainViewModel)
        }

        /// <summary>Test stub for set_WelcomeTitle(String)</summary>
        [global::Microsoft.Pex.Framework.PexMethod]
        public void WelcomeTitleSet([global::Microsoft.Pex.Framework.PexAssumeUnderTest]global::SharpBlocks.Net.ViewModel.MainViewModel target, string value)
        {
            target.WelcomeTitle = value;
            // TODO: add assertions to method MainViewModelTest.WelcomeTitleSet(MainViewModel, String)
        }
    }
}
