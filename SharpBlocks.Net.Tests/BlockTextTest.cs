﻿using SharpBlocks.Net.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using SharpBlocks.Net.Core.CodeBlocks;

namespace SharpBlocks.Net.Tests
{
    
    
    /// <summary>
    ///This is a test class for BlockTextTest and is intended
    ///to contain all BlockTextTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BlockTextTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for BlockText Constructor
        ///</summary>
        [TestMethod()]
        public void BlockTextConstructorTest()
        {
            BlockText target = new BlockText();
            Assert.IsNotNull(target);
            //Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for Items
        ///</summary>
        [TestMethod()]
        public void ItemsTest()
        {
            BlockText target = new BlockText(); // TODO: Initialize to an appropriate value
            object[] expected = null; // TODO: Initialize to an appropriate value
            object[] actual;
            target.Items = expected;
            actual = target.Items;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ItemsElementName
        ///</summary>
        [TestMethod()]
        public void ItemsElementNameTest()
        {
            BlockText target = new BlockText(); // TODO: Initialize to an appropriate value
            ItemsChoiceType[] expected = null; // TODO: Initialize to an appropriate value
            ItemsChoiceType[] actual;
            target.ItemsElementName = expected;
            actual = target.ItemsElementName;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Text
        ///</summary>
        [TestMethod()]
        public void TextTest()
        {
            BlockText target = new BlockText(); // TODO: Initialize to an appropriate value
            List<string> expected = new List<string>(); // TODO: Initialize to an appropriate value
            List<string> actual;
            target.Text = expected;
            actual = target.Text;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
