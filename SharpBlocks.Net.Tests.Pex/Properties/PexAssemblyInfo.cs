// <copyright file="PexAssemblyInfo.cs">Copyright �  2013</copyright>
// Microsoft.Pex.Framework.Settings
[assembly: global::Microsoft.Pex.Framework.Settings.PexAssemblySettings(TestFramework = "VisualStudioUnitTest")]

// Microsoft.Pex.Framework.Instrumentation
[assembly: global::Microsoft.Pex.Framework.Instrumentation.PexAssemblyUnderTest("SharpBlocks.Net")]
[assembly: global::Microsoft.Pex.Framework.Instrumentation.PexInstrumentAssembly("GalaSoft.MvvmLight.Extras.WPF4")]
[assembly: global::Microsoft.Pex.Framework.Instrumentation.PexInstrumentAssembly("WindowsBase")]
[assembly: global::Microsoft.Pex.Framework.Instrumentation.PexInstrumentAssembly("System.Core")]
[assembly: global::Microsoft.Pex.Framework.Instrumentation.PexInstrumentAssembly("Microsoft.Practices.ServiceLocation")]
[assembly: global::Microsoft.Pex.Framework.Instrumentation.PexInstrumentAssembly("PresentationFramework")]
[assembly: global::Microsoft.Pex.Framework.Instrumentation.PexInstrumentAssembly("GalaSoft.MvvmLight.WPF4")]
[assembly: global::Microsoft.Pex.Framework.Instrumentation.PexInstrumentAssembly("System.Xaml")]

// Microsoft.Pex.Framework.Creatable
[assembly: global::Microsoft.Pex.Framework.Creatable.PexCreatableFactoryForDelegates]

// Microsoft.Pex.Framework.Validation
[assembly: global::Microsoft.Pex.Framework.Validation.PexAllowedContractRequiresFailureAtTypeUnderTestSurface]
[assembly: global::Microsoft.Pex.Framework.Validation.PexAllowedXmlDocumentedException]

// Microsoft.Pex.Framework.Coverage
[assembly: global::Microsoft.Pex.Framework.Coverage.PexCoverageFilterAssembly(global::Microsoft.Pex.Framework.Coverage.PexCoverageDomain.UserOrTestCode, "GalaSoft.MvvmLight.Extras.WPF4")]
[assembly: global::Microsoft.Pex.Framework.Coverage.PexCoverageFilterAssembly(global::Microsoft.Pex.Framework.Coverage.PexCoverageDomain.UserOrTestCode, "WindowsBase")]
[assembly: global::Microsoft.Pex.Framework.Coverage.PexCoverageFilterAssembly(global::Microsoft.Pex.Framework.Coverage.PexCoverageDomain.UserOrTestCode, "System.Core")]
[assembly: global::Microsoft.Pex.Framework.Coverage.PexCoverageFilterAssembly(global::Microsoft.Pex.Framework.Coverage.PexCoverageDomain.UserOrTestCode, "Microsoft.Practices.ServiceLocation")]
[assembly: global::Microsoft.Pex.Framework.Coverage.PexCoverageFilterAssembly(global::Microsoft.Pex.Framework.Coverage.PexCoverageDomain.UserOrTestCode, "PresentationFramework")]
[assembly: global::Microsoft.Pex.Framework.Coverage.PexCoverageFilterAssembly(global::Microsoft.Pex.Framework.Coverage.PexCoverageDomain.UserOrTestCode, "GalaSoft.MvvmLight.WPF4")]
[assembly: global::Microsoft.Pex.Framework.Coverage.PexCoverageFilterAssembly(global::Microsoft.Pex.Framework.Coverage.PexCoverageDomain.UserOrTestCode, "System.Xaml")]

// Microsoft.Pex.Framework.Moles
[assembly: global::Microsoft.Pex.Framework.Moles.PexAssumeContractEnsuresFailureAtBehavedSurface]
[assembly: global::Microsoft.Pex.Framework.Moles.PexChooseAsBehavedCurrentBehavior]

// Microsoft.Pex.Linq
[assembly: global::Microsoft.Pex.Linq.PexLinqPackage]

