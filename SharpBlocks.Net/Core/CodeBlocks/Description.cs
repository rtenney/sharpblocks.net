﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace SharpBlocks.Net.Core.CodeBlocks
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [DesignerCategory(@"code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(ElementName = "description", Namespace = "", IsNullable = false)]
    public class Description
    {
        private List<ArgumentDescription> _argumentDescription;
        private BlockText _text;

        [XmlElement("arg-description", Order = 1)]
        public List<ArgumentDescription> ArgumentDescription
        {
            get
            {
                if ((_argumentDescription == null))
                {
                    _argumentDescription = new List<ArgumentDescription>();
                }
                return _argumentDescription;
            }
            set
            {
                if ((_argumentDescription != null))
                {
                    if ((_argumentDescription.Equals(value) != true))
                    {
                        _argumentDescription = value;
                    }
                }
                else
                {
                    _argumentDescription = value;
                }
            }
        }

        [XmlElement(ElementName = "text", Order = 0)]
        public BlockText Text
        {
            get
            {
                if ((_text == null))
                {
                    _text = new BlockText();
                }
                return _text;
            }
            set
            {
                if ((_text != null))
                {
                    if ((Text.Equals(value)))
                    {
                        _text = value;
                    }
                }
                else
                {
                    _text = value;
                }
            }
        }
    }
}