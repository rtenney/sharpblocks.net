﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace SharpBlocks.Net.Core.Workspace
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [DesignerCategory(@"code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class TrashCan //: EntityBase<TrashCan>
    {
        
        private string _closedTrashImage;
        private string _openTrashImage;

        [XmlElement(Order = 0)]
        public string OpenTrashImage
        {
            get { return _openTrashImage; }
            set
            {
                if ((_openTrashImage != null))
                {
                    if ((_openTrashImage.Equals(value) != true))
                    {
                        _openTrashImage = value;
                        //OnPropertyChanged("OpenTrashImage");
                    }
                }
                else
                {
                    _openTrashImage = value;
                    //OnPropertyChanged("OpenTrashImage");
                }
            }
        }

        [XmlElement(Order = 1)]
        public string ClosedTrashImage
        {
            get { return _closedTrashImage; }
            set
            {
                if ((_closedTrashImage != null))
                {
                    if ((_closedTrashImage.Equals(value) != true))
                    {
                        _closedTrashImage = value;
                        //OnPropertyChanged("ClosedTrashImage");
                    }
                }
                else
                {
                    _closedTrashImage = value;
                    //OnPropertyChanged("ClosedTrashImage");
                }
            }
        }
    }
}