﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace SharpBlocks.Net.Core.CodeBlocks
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [DesignerCategory(@"code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class BlockConnectorShapes //: EntityBase<BlockConnectorShapes>
    {
        
        private List<BlockConnectorShape> _blockConnectorShape;

        [XmlElement("BlockConnectorShape", Order = 0)]
        public List<BlockConnectorShape> BlockConnectorShape
        {
            get
            {
                if ((_blockConnectorShape == null))
                {
                    _blockConnectorShape = new List<BlockConnectorShape>();
                }
                return _blockConnectorShape;
            }
            set
            {
                if ((_blockConnectorShape != null))
                {
                    if ((_blockConnectorShape.Equals(value) != true))
                    {
                        _blockConnectorShape = value;
                        //OnPropertyChanged("BlockConnectorShape");
                    }
                }
                else
                {
                    _blockConnectorShape = value;
                    //OnPropertyChanged("BlockConnectorShape");
                }
            }
        }
    }
}