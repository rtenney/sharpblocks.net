﻿using SharpBlocks.Net.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SharpBlocks.Net.Core.CodeBlocks;

namespace SharpBlocks.Net.Tests
{
    
    
    /// <summary>
    ///This is a test class for BlockConnectorTest and is intended
    ///to contain all BlockConnectorTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BlockConnectorTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for BlockConnector Constructor
        ///</summary>
        [TestMethod()]
        public void BlockConnectorConstructorTest()
        {
            BlockConnector target = new BlockConnector();
            Assert.IsNotNull(target);
            //Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for ConnectorType
        ///</summary>
        [TestMethod()]
        public void ConnectorTypeTest()
        {
            BlockConnector target = new BlockConnector(); // TODO: Initialize to an appropriate value
            string expected = "string"; // TODO: Initialize to an appropriate value
            string actual;
            target.ConnectorType = expected;
            actual = target.ConnectorType;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for DefaultArgument
        ///</summary>
        [TestMethod()]
        public void DefaultArgumentTest()
        {
            BlockConnector target = new BlockConnector(); // TODO: Initialize to an appropriate value
            DefaultArgument expected = new DefaultArgument() { Label = "test" }; // TODO: Initialize to an appropriate value
            DefaultArgument actual;
            target.DefaultArgument = expected;
            actual = target.DefaultArgument;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsExpandable
        ///</summary>
        [TestMethod()]
        public void IsExpandableTest()
        {
            BlockConnector target = new BlockConnector(); // TODO: Initialize to an appropriate value
            string expected = "yes"; // TODO: Initialize to an appropriate value
            string actual;
            target.IsExpandableString = expected;
            actual = target.IsExpandableString;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsLabelEditable
        ///</summary>
        [TestMethod()]
        public void IsLabelEditableTest()
        {
            BlockConnector target = new BlockConnector(); // TODO: Initialize to an appropriate value
            string expected = "yes"; // TODO: Initialize to an appropriate value
            string actual;
            target.IsLabelEditableString = expected;
            actual = target.IsLabelEditableString;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Kind
        ///</summary>
        [TestMethod()]
        public void KindTest()
        {
            BlockConnector target = new BlockConnector(); // TODO: Initialize to an appropriate value
            BlockConnector.ConnectorKind expected = BlockConnector.ConnectorKind.plug; // TODO: Initialize to an appropriate value
            BlockConnector.ConnectorKind actual;
            target.Kind = expected;
            actual = target.Kind;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for BlockLabel
        ///</summary>
        [TestMethod()]
        public void LabelTest()
        {
            BlockConnector target = new BlockConnector(); // TODO: Initialize to an appropriate value
            string expected = "test"; // TODO: Initialize to an appropriate value
            string actual;
            target.Label = expected;
            actual = target.Label;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Position_Type
        ///</summary>
        [TestMethod()]
        public void Position_TypeTest()
        {
            BlockConnector target = new BlockConnector(); // TODO: Initialize to an appropriate value
            BlockConnector.PositionType expected = BlockConnector.PositionType.bottom; // TODO: Initialize to an appropriate value
            BlockConnector.PositionType actual;
            target.Position_Type = expected;
            actual = target.Position_Type;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
