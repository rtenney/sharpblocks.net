﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace SharpBlocks.Net.Core.CodeBlocks
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [DesignerCategory(@"code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class LangSpecProperties //: EntityBase<LangSpecProperties>
    {
        
        private List<LangSpecProperty> _langSpecProperty;

        [XmlElement("LangSpecProperty", Order = 0)]
        public List<LangSpecProperty> LangSpecProperty
        {
            get
            {
                if ((_langSpecProperty == null))
                {
                    _langSpecProperty = new List<LangSpecProperty>();
                }
                return _langSpecProperty;
            }
            set
            {
                if ((_langSpecProperty != null))
                {
                    if ((_langSpecProperty.Equals(value) != true))
                    {
                        _langSpecProperty = value;
                        //OnPropertyChanged("LangSpecProperty");
                    }
                }
                else
                {
                    _langSpecProperty = value;
                    //OnPropertyChanged("LangSpecProperty");
                }
            }
        }
    }
}