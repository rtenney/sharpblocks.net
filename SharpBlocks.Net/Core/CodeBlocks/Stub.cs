﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace SharpBlocks.Net.Core.CodeBlocks
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [DesignerCategory(@"code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class Stub
    {
        private List<LangSpecProperty> _langSpecProperties;

        private string _scope;

        private StubGenuses _stubgenus;

        [XmlArray(Order = 0)]
        [XmlArrayItem("LangSpecProperty", IsNullable = false)]
        public List<LangSpecProperty> LangSpecProperties
        {
            get
            {
                if ((_langSpecProperties == null))
                {
                    _langSpecProperties = new List<LangSpecProperty>();
                }
                return _langSpecProperties;
            }
            set
            {
                if ((_langSpecProperties != null))
                {
                    if ((_langSpecProperties.Equals(value) != true))
                    {
                        _langSpecProperties = value;
                    }
                }
                else
                {
                    _langSpecProperties = value;
                }
            }
        }

        [XmlAttribute("scope")]
        public string Scope
        {
            get { return _scope; }
            set
            {
                if ((_scope != null))
                {
                    if ((_scope.Equals(value) != true))
                    {
                        _scope = value;
                    }
                }
                else
                {
                    _scope = value;
                }
            }
        }

        [XmlAttribute("stub-genus")]
        public StubGenuses StubGenus
        {
            get { return _stubgenus; }
            set
            {
                if (_stubgenus.Equals(value)) return;
                _stubgenus = value;
            }
        }

        [GeneratedCode("System.Xml", "4.0.30319.18213")]
        [Serializable]
        [XmlType(AnonymousType = true)]
        public enum StubGenuses
        {
            /// <remarks />
            getter,

            /// <remarks />
            setter,

            /// <remarks />
            caller,

            /// <remarks />
            agent,

            /// <remarks />
            inc,
        }
    }
}