﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Xml.Serialization;
using SharpBlocks.Net.Core.Collections;
using SharpBlocks.Net.Core.Utilities;

namespace SharpBlocks.Net.Core.CodeBlocks
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [DesignerCategory(@"code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class BlockGenus
    {
        private static Dictionary<string, BlockGenus> _nameToGenus = new Dictionary<string, BlockGenus>();
        private List<string> _argumentDescriptions = new List<string>();
        private List<BlockConnector> _blockConnectors;
        private Description _blockDescription;
        private Dictionary<ImageLocation, BlockImageIcon> _blockImageMap;
        private string _color;
        private List<List<BlockConnector>> _expandGroups = new List<List<BlockConnector>>();
        private string _genusName;
        private Images _images;
        private string _initlabel;
        private BlockGenusKinds _kind;
        private string _labelprefix;
        private string _labelsuffix;
        private List<LangSpecProperty> _langSpecProperties;
        private StubCollection _stubs;

        public BlockGenus()
        {
            IsLabelEditable = false;
            IsLabelUnique = false;
            IsLabelValue = false;
            IsPageLabelEnabled = false;
            IsStarter = false;
            IsTerminator = false;
        }

        internal BlockGenus(string genusName, string newGenusName)
            : this()
        {
            System.Diagnostics.Debug.Assert(!genusName.Equals(newGenusName), String.Format("BlockGenuses must have unique names: {0}", genusName));

            BlockGenus genusToCopy = BlockGenus.GetGenusWithName(genusName);

            this.GenusName = newGenusName;
            this.AreSocketsExpandable = genusToCopy.AreSocketsExpandable;
            this.Color = genusToCopy.Color;
            this.FamilyList = new List<string>(genusToCopy.FamilyList ?? new List<string>());
            this.HasDefaultArguments = genusToCopy.HasDefaultArguments;
            this.InitialLabel = genusToCopy.InitialLabel;
            this.IsLabelEditable = genusToCopy.IsLabelEditable;
            this.IsLabelValue = genusToCopy.IsLabelValue;
            this.IsStarter = genusToCopy.IsStarter;
            this.IsTerminator = genusToCopy.IsTerminator;
            this.BlockConnectors = genusToCopy.BlockConnectors;

            //this.IsInfix = genusToCopy.IsInfix;
            this.Kind = genusToCopy.Kind;
            this.LabelPrefix = genusToCopy.LabelPrefix;
            this.LabelSuffix = genusToCopy.LabelSuffix;

            //if (genusToCopy.Plug != null)
            //    this.Plug = new BlockConnector(genusToCopy.Plug);
            if (genusToCopy.Before != null)
                this.Before = new BlockConnector(genusToCopy.Before);
            if (genusToCopy.After != null)
                this.After = new BlockConnector(genusToCopy.After);
            this.Properties = new Dictionary<string, string>(genusToCopy.Properties ?? new Dictionary<string, string>());

            //this.Sockets = new List<BlockConnector>(genusToCopy.Sockets ?? new List<BlockConnector>());
            this.StubList = new List<string>(genusToCopy.StubList ?? new List<string>());
            this._expandGroups = genusToCopy.ExpandGroups;   // doesn't change
        }

        public static BlockGenus GetGenusWithName(string genusName)
        {
            Debug.Assert(BlockGenus.NameToGenus.ContainsKey(genusName), "Genus name does not exists in dictionary.");
            return BlockGenus.NameToGenus[genusName];
        }

        public static void ResetAllGenuses()
        {
            _nameToGenus.Clear();
        }

        public string GetProperty(string property)
        {
            Debug.Assert(Properties.ContainsKey(property), "Property does not exists in dictionary.");
            return Properties[property];
        }

        public override string ToString()
        {
            return "BlockGenus Name: ("
                + GenusName
                + "), Kind: (" + Kind + ")"
                + (BlockConnectors != null && BlockConnectors.Count > 0 ? ", Connectors: (" + BlockConnectors.Count.ToString() + ")" : "")
                + (Plug != null ? ", (IsPlug)" : "")
                + (Sockets != null && Sockets.Count > 0 ? ", Sockets: (" + Sockets.Count.ToString() + ")" : "")
                + (Stubs.Count > 0 ? ", Stubs: (" + Stubs.Count.ToString() + ")" : "");
        }

        #region Properties

        private List<string> _stubList;

        [XmlIgnore]
        public static Dictionary<string, BlockGenus> NameToGenus
        {
            get
            {
                if (_nameToGenus == null)
                    _nameToGenus = new Dictionary<string, BlockGenus>();
                return _nameToGenus;
            }
        }

        [XmlIgnore]
        public BlockConnector After { get; set; }

        [XmlIgnore]
        public bool AreSocketsExpandable { get; set; }

        [XmlIgnore]
        public List<string> ArgumentDescriptions
        {
            get
            {
                return _argumentDescriptions;
            }
        }

        [XmlIgnore]
        public BlockConnector Before { get; set; }

        [XmlArray(Order = 1)]
        [XmlArrayItem("BlockConnector", IsNullable = false)]
        public List<BlockConnector> BlockConnectors
        {
            get
            {
                if ((_blockConnectors == null))
                {
                    _blockConnectors = new List<BlockConnector>();
                }
                return _blockConnectors;
            }
            set
            {
                if ((_blockConnectors != null))
                {
                    if ((_blockConnectors.Equals(value) != true))
                    {
                        _blockConnectors = value;
                    }
                }
                else
                {
                    _blockConnectors = value;
                }
            }
        }

        [XmlElement(ElementName = "description", Order = 0)]
        public Description BlockDescription
        {
            get
            {
                if ((_blockDescription == null))
                {
                    _blockDescription = new Description();
                }
                return _blockDescription;
            }
            set
            {
                if ((_blockDescription != null))
                {
                    if ((_blockDescription.Equals(value) != true))
                    {
                        _blockDescription = value;
                    }
                }
                else
                {
                    _blockDescription = value;
                }
            }
        }

        [XmlAttribute("color")]
        public string Color
        {
            get
            {
                return _color;
            }
            set
            {
                if ((_color != null))
                {
                    if ((_color.Equals(value) != true))
                    {
                        _color = value;
                    }
                }
                else
                {
                    _color = value;
                }
            }
        }

        [XmlAttribute("editable-label")]
        [DefaultValue("no")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string EditableLabelString
        {
            get
            {
                return XmlTypeConverter.ConvertFromBoolean(IsLabelEditable);
            }
            set
            {
                if ((IsLabelEditable.Equals(value) != true))
                {
                    IsLabelEditable = XmlTypeConverter.ConvertToBoolean(value);
                }
            }
        }

        [XmlIgnore]
        public List<List<BlockConnector>> ExpandGroups
        {
            get
            {
                if (_expandGroups == null)
                    _expandGroups = new List<List<BlockConnector>>();
                return _expandGroups;
            }
        }

        [XmlIgnore]
        public List<string> FamilyList { get; set; }

        [XmlAttribute("name")]
        public string GenusName
        {
            get
            {
                return _genusName;
            }
            set
            {
                if ((_genusName != null))
                {
                    if ((_genusName.Equals(value) != true))
                    {
                        _genusName = value;
                    }
                }
                else
                {
                    _genusName = value;
                }
            }
        }

        [XmlIgnore]
        public bool HasAfterConnector
        {
            get
            {
                return !IsTerminator;
            }
        }

        [XmlIgnore]
        public bool HasBeforeConnector
        {
            get
            {
                return !IsStarter;
            }
        }

        [XmlIgnore]
        public bool HasDefaultArguments { get; set; }

        [XmlIgnore]
        public bool HasSiblings
        {
            get
            {
                if (FamilyList != null)
                    FamilyList = new List<string>();
                return (FamilyList.Count > 0);
            }
        }

        [XmlElement(Order = 3)]
        public Images Images
        {
            get
            {
                if ((_images == null))
                {
                    _images = new Images();
                }
                return _images;
            }
            set
            {
                if ((_images != null))
                {
                    if ((_images.Equals(value) != true))
                    {
                        _images = value;
                    }
                }
                else
                {
                    _images = value;
                }
            }
        }

        [XmlIgnore]
        public Dictionary<ImageLocation, BlockImageIcon> InitialBlockImageMap
        {
            get
            {
                return _blockImageMap;
            }
        }

        [XmlAttribute("initlabel")]
        public string InitialLabel
        {
            get
            {
                return _initlabel;
            }
            set
            {
                if ((_initlabel != null))
                {
                    if ((_initlabel.Equals(value) != true))
                    {
                        _initlabel = value;
                    }
                }
                else
                {
                    _initlabel = value;
                }
            }
        }

        [XmlIgnore]
        public bool IsCommandBlock
        {
            get
            {
                return Kind == BlockGenusKinds.command;
            }
        }

        [XmlIgnore]
        public bool IsDataBlock
        {
            get
            {
                return Kind == BlockGenusKinds.data;
            }
        }

        [XmlIgnore]
        public bool IsDeclaration
        {
            get
            {
                return Kind == BlockGenusKinds.procedure || Kind == BlockGenusKinds.variable;
            }
        }

        [XmlIgnore]
        public bool IsFunctionBlock
        {
            get
            {
                return Kind == BlockGenusKinds.function;
            }
        }

        [XmlIgnore]
        public bool IsInfix
        {
            get
            {
                return BlockConnectors.Count(x => x.Position_Type == BlockConnector.PositionType.bottom && Sockets.Count == 2) == 2;
            }
        }

        [XmlIgnore]
        public bool IsLabelEditable { get; set; }

        [XmlIgnore]
        public bool IsLabelUnique { get; set; }

        [XmlAttribute("label-unique")]
        [DefaultValue("no")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string IsLabelUniqueString
        {
            get
            {
                return XmlTypeConverter.ConvertFromBoolean(IsLabelUnique);
            }
            set
            {
                if ((IsLabelUnique.Equals(value) != true))
                {
                    IsLabelUnique = XmlTypeConverter.ConvertToBoolean(value);
                }
            }
        }

        [XmlIgnore]
        public bool IsLabelValue { get; set; }

        [XmlAttribute("is-label-value")]
        [DefaultValue("no")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string IsLabelValueString
        {
            get
            {
                return XmlTypeConverter.ConvertFromBoolean(IsLabelValue);
            }
            set
            {
                if ((IsLabelValue.Equals(value) != true))
                {
                    IsLabelValue = XmlTypeConverter.ConvertToBoolean(value);
                }
            }
        }

        [XmlIgnore]
        public bool IsListRelated
        {
            get
            {
                bool hasListConn = false;
                if (Plug != null)
                    hasListConn = Plug.Kind == BlockConnector.ConnectorKind.list;
                foreach (BlockConnector socket in Sockets)
                    hasListConn |= socket.Kind == BlockConnector.ConnectorKind.list;
                return hasListConn;
            }
        }

        [XmlIgnore]
        public bool IsPageLabelEnabled { get; set; }

        [XmlAttribute("page-label-enabled")]
        [DefaultValue("no")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string IsPageLabelEnabledString
        {
            get
            {
                return XmlTypeConverter.ConvertFromBoolean(IsPageLabelEnabled);
            }
            set
            {
                if ((IsPageLabelEnabled.Equals(value) != true))
                {
                    IsPageLabelEnabled = XmlTypeConverter.ConvertToBoolean(value);
                }
            }
        }

        [XmlIgnore]
        public bool IsProcedureDeclBlock
        {
            get
            {
                return Kind == BlockGenusKinds.procedure;
            }
        }

        [XmlIgnore]
        public bool IsProcedureParamBlock
        {
            get
            {
                return Kind == BlockGenusKinds.param;
            }
        }

        [XmlIgnore]
        public bool IsStarter { get; set; }

        [XmlAttribute("is-starter")]
        [DefaultValue("no")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string IsStarterString
        {
            get
            {
                return XmlTypeConverter.ConvertFromBoolean(IsStarter);
            }
            set
            {
                if ((IsStarter.Equals(value) != true))
                {
                    IsStarter = XmlTypeConverter.ConvertToBoolean(value);
                }
            }
        }

        [XmlIgnore]
        public bool IsTerminator { get; set; }

        [XmlAttribute("is-terminator")]
        [DefaultValue("no")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string IsTerminatorString
        {
            get
            {
                return XmlTypeConverter.ConvertFromBoolean(IsTerminator);
            }
            set
            {
                if ((IsTerminator.Equals(value) != true))
                {
                    IsTerminator = XmlTypeConverter.ConvertToBoolean(value);
                }
            }
        }

        [XmlIgnore]
        public bool IsVariableDeclBlock
        {
            get
            {
                return Kind == BlockGenusKinds.variable;
            }
        }

        [XmlAttribute("kind")]
        public BlockGenusKinds Kind
        {
            get
            {
                return _kind;
            }
            set
            {
                if ((_kind.Equals(value) != true))
                {
                    _kind = value;
                }
            }
        }

        [XmlAttribute("label-prefix")]
        public string LabelPrefix
        {
            get
            {
                return _labelprefix;
            }
            set
            {
                if ((_labelprefix != null))
                {
                    if ((_labelprefix.Equals(value) != true))
                    {
                        _labelprefix = value;
                    }
                }
                else
                {
                    _labelprefix = value;
                }
            }
        }

        [XmlAttribute("label-suffix")]
        public string LabelSuffix
        {
            get
            {
                return _labelsuffix;
            }
            set
            {
                if ((_labelsuffix != null))
                {
                    if ((_labelsuffix.Equals(value) != true))
                    {
                        _labelsuffix = value;
                    }
                }
                else
                {
                    _labelsuffix = value;
                }
            }
        }

        [XmlArray(Order = 4)]
        [XmlArrayItem("LangSpecProperty", IsNullable = false)]
        public List<LangSpecProperty> LangSpecProperties
        {
            get
            {
                if ((_langSpecProperties == null))
                {
                    _langSpecProperties = new List<LangSpecProperty>();
                }
                return _langSpecProperties;
            }
            set
            {
                if ((_langSpecProperties != null))
                {
                    if ((_langSpecProperties.Equals(value) != true))
                    {
                        _langSpecProperties = value;
                    }
                }
                else
                {
                    _langSpecProperties = value;
                }
            }
        }

        [XmlIgnore]
        public BlockConnector Plug
        {
            get
            {
                return BlockConnectors.SingleOrDefault(x => x.Kind == BlockConnector.ConnectorKind.plug);
            }
        }

        [XmlIgnore]
        public Dictionary<string, string> Properties { get; set; }

        [XmlIgnore]
        public List<BlockConnector> Sockets
        {
            get
            {
                return BlockConnectors.Where(x => x.Kind == BlockConnector.ConnectorKind.socket).ToList();
            }
        }

        [XmlIgnore]
        public List<string> StubList
        {
            get
            {
                if (_stubList == null)
                    _stubList = new List<string>();
                return _stubList;
            }
            set
            {
                _stubList = value;
            }
        }

        [XmlArray(Order = 2)]
        [XmlArrayItem("Stub", IsNullable = false)]
        public StubCollection Stubs
        {
            get
            {
                if ((_stubs == null))
                {
                    _stubs = new StubCollection(this);
                }
                return _stubs;
            }
            set
            {
                if ((_stubs != null))
                {
                    if ((_stubs.Equals(value) != true))
                    {
                        _stubs = value;
                    }
                }
                else
                {
                    _stubs = value;
                }
            }
        }

        public bool ShouldSerializeBlockConnectors()
        {
            return this.BlockConnectors.Count > 0;
        }

        public bool ShouldSerializeBlockDescription()
        {
            return this.BlockDescription.Text.Text.Count > 0;
        }

        public bool ShouldSerializeImages()
        {
            return this.Images.Image.Width != 0 && this.Images.Image.Height != 0;
        }

        public bool ShouldSerializeLangSpecProperties()
        {
            return this.LangSpecProperties.Count > 0;
        }

        public bool ShouldSerializeStubs()
        {
            return this.Stubs.Count != 0;
        }

        #endregion Properties

        private static void AddToExpandGroup(List<List<BlockConnector>> groups, BlockConnector socket)
        {
            List<BlockConnector> eGroup = GetExpandGroup(groups, socket.ExpandGroup);
            if (eGroup == null)
            {
                eGroup = new List<BlockConnector>();
                groups.Add(eGroup);
            }
            eGroup.Add(new BlockConnector(socket));
        }

        private static List<BlockConnector> GetExpandGroup(List<List<BlockConnector>> groups, string group)
        {
            foreach (List<BlockConnector> list in groups)
            {
                // Always at least one element in the group.
                if (list[0].ExpandGroup == group)
                {
                    return list;
                }
            }

            return null;
        }
    }
}