﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace SharpBlocks.Net.Core.CodeBlocks
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [DesignerCategory(@"code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(ElementName = "DefaultArg", Namespace = "", IsNullable = false)]
    public sealed class DefaultArgument
    {
        private string _genusName;
        private string _label;

        public DefaultArgument()
        {
        }

        public DefaultArgument(string genusName, string label)
            : this()
        {
            this.GenusName = genusName;
            this.Label = label;
        }

        [XmlAttribute("genus-name")]
        public string GenusName
        {
            get
            {
                return _genusName;
            }
            set
            {
                if ((_genusName != null))
                {
                    if ((_genusName.Equals(value) != true))
                    {
                        _genusName = value;
                    }
                }
                else
                {
                    _genusName = value;
                }
            }
        }

        [XmlAttribute("label")]
        public string Label
        {
            get
            {
                return _label;
            }
            set
            {
                if ((_label != null))
                {
                    if ((_label.Equals(value) != true))
                    {
                        _label = value;
                    }
                }
                else
                {
                    _label = value;
                }
            }
        }

        public static string Escape(string s)
        {
            return s.Replace("&", "&amp;")
                    .Replace("<", "&lt;")
                    .Replace(">", "&gt;");
        }
    }
}