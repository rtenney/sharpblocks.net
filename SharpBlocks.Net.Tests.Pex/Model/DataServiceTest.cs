// <copyright file="DataServiceTest.cs">Copyright �  2013</copyright>
namespace SharpBlocks.Net.Model
{
    /// <summary>This class contains parameterized unit tests for DataService</summary>
    [global::Microsoft.Pex.Framework.PexClass(typeof(global::SharpBlocks.Net.Model.DataService))]
    [global::Microsoft.Pex.Framework.Validation.PexAllowedExceptionFromTypeUnderTest(typeof(global::System.InvalidOperationException))]
    [global::Microsoft.Pex.Framework.Validation.PexAllowedExceptionFromTypeUnderTest(typeof(global::System.ArgumentException), AcceptExceptionSubtypes = true)]
    [global::Microsoft.VisualStudio.TestTools.UnitTesting.TestClass]
    public partial class DataServiceTest
    {
        /// <summary>Test stub for GetData(Action`2&lt;DataItem,Exception&gt;)</summary>
        [global::Microsoft.Pex.Framework.PexMethod]
        public void GetData(
            [global::Microsoft.Pex.Framework.PexAssumeUnderTest]global::SharpBlocks.Net.Model.DataService target,
            global::System.Action<global::SharpBlocks.Net.Model.DataItem, global::System.Exception> callback
        )
        {
            target.GetData(callback);
            // TODO: add assertions to method DataServiceTest.GetData(DataService, Action`2<DataItem,Exception>)
        }
    }
}
