﻿using SharpBlocks.Net.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using SharpBlocks.Net.Core.CodeBlocks;

namespace SharpBlocks.Net.Tests
{
    
    
    /// <summary>
    ///This is a test class for BlockConnectorShapesTest and is intended
    ///to contain all BlockConnectorShapesTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BlockConnectorShapesTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for BlockConnectorShapes Constructor
        ///</summary>
        [TestMethod()]
        public void BlockConnectorShapesConstructorTest()
        {
            BlockConnectorShapes target = new BlockConnectorShapes();
            Assert.IsNotNull(target);
            //Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for BlockConnectorShape
        ///</summary>
        [TestMethod()]
        public void BlockConnectorShapeTest()
        {
            BlockConnectorShapes target = new BlockConnectorShapes(); // TODO: Initialize to an appropriate value
            List<BlockConnectorShape> expected = new List<BlockConnectorShape>(); // TODO: Initialize to an appropriate value
            List<BlockConnectorShape> actual;
            target.BlockConnectorShape = expected;
            actual = target.BlockConnectorShape;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
