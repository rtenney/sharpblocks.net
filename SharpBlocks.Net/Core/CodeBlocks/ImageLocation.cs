﻿using System;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

namespace SharpBlocks.Net.Core.CodeBlocks
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [XmlType("ImageBlockLocation", AnonymousType = true)]
    public enum ImageLocation
    {
        /// <remarks />
        center,

        /// <remarks />
        east,

        /// <remarks />
        west,

        /// <remarks />
        north,

        /// <remarks />
        south,

        /// <remarks />
        southeast,

        /// <remarks />
        southwest,

        /// <remarks />
        northeast,

        /// <remarks />
        northwest,
    }
}