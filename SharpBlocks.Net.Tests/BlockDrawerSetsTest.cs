﻿using SharpBlocks.Net.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using SharpBlocks.Net.Core.Workspace;

namespace SharpBlocks.Net.Tests
{
    
    
    /// <summary>
    ///This is a test class for BlockDrawerSetsTest and is intended
    ///to contain all BlockDrawerSetsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BlockDrawerSetsTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for BlockDrawerSets Constructor
        ///</summary>
        [TestMethod()]
        public void BlockDrawerSetsConstructorTest()
        {
            BlockDrawerSets target = new BlockDrawerSets();
            Assert.IsNotNull(target);
            //Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for BlockDrawerSet
        ///</summary>
        [TestMethod()]
        public void BlockDrawerSetTest()
        {
            BlockDrawerSets target = new BlockDrawerSets(); // TODO: Initialize to an appropriate value
            List<BlockDrawerSet> expected = new List<BlockDrawerSet>(); // TODO: Initialize to an appropriate value
            expected.Add(new BlockDrawerSet());
            List<BlockDrawerSet> actual;
            target.BlockDrawerSet = expected;
            actual = target.BlockDrawerSet;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
