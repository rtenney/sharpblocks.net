﻿using SharpBlocks.Net.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Xml.Serialization;

namespace SharpBlocks.Net.Tests
{
    
    
    /// <summary>
    ///This is a test class for EntityBaseTest and is intended
    ///to contain all EntityBaseTest Unit Tests
    ///</summary>
    [TestClass()]
    public class EntityBaseTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for EntityBase`1 Constructor
        ///</summary>
        public void EntityBaseConstructorTestHelper<T>()
        {
            EntityBase<T> target = new EntityBase<T>();
            Assert.IsNotNull(target);
            //Assert.Inconclusive("TODO: Implement code to verify target");
        }

        [TestMethod()]
        public void EntityBaseConstructorTest()
        {
            EntityBaseConstructorTestHelper<GenericParameterHelper>();
        }

        ///// <summary>
        /////A test for Clone
        /////</summary>
        //public void CloneTestHelper<T>()
        //{
        //    EntityBase<T> target = new EntityBase<T>(); // TODO: Initialize to an appropriate value
        //    //T expected = default(T); // TODO: Initialize to an appropriate value
        //    EntityBase<T> actual;
        //    actual = target.Clone();
        //    Assert.AreEqual(target, actual);
        //    //Assert.Inconclusive("Verify the correctness of this test method.");
        //}

        //[TestMethod()]
        //public void CloneTest()
        //{
        //    CloneTestHelper<BlockGenus>();
        //}

        /// <summary>
        ///A test for OnPropertyChanged
        ///</summary>
        public void OnPropertyChangedTestHelper<T>()
        {
            OnPropertyChangeHelperClass<GenericParameterHelper> temp = new OnPropertyChangeHelperClass<GenericParameterHelper>();
            temp.TestProperty = "Test";
            Assert.AreEqual(temp.Property, "TestProperty");
        }

        [TestMethod()]
        public void OnPropertyChangedTest()
        {
            OnPropertyChangedTestHelper<GenericParameterHelper>();
        }
    }

    public class OnPropertyChangeHelperClass<T>: EntityBase<T>
    {
        public string Property = string.Empty;
        private string _testProperty = string.Empty;
        public string TestProperty
        {
            get
            {
                return _testProperty;
            }
            set
            {
                if (_testProperty != value)
                {
                    _testProperty = value;
                    OnPropertyChanged("TestProperty");
                }
            }
        }

        public OnPropertyChangeHelperClass()
        {
            PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(temp_PropertyChanged);
        }

        void temp_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            Property = e.PropertyName;
        }
    }
}
