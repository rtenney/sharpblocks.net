﻿using SharpBlocks.Net.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using SharpBlocks.Net.Core.CodeBlocks;

namespace SharpBlocks.Net.Tests
{
    
    
    /// <summary>
    ///This is a test class for DescriptionTest and is intended
    ///to contain all DescriptionTest Unit Tests
    ///</summary>
    [TestClass()]
    public class DescriptionTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Description Constructor
        ///</summary>
        [TestMethod()]
        public void DescriptionConstructorTest()
        {
            Description target = new Description();
            Assert.IsNotNull(target);
            //Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for ArgumentDescription
        ///</summary>
        [TestMethod()]
        public void ArgumentDescriptionTest()
        {
            Description target = new Description(); // TODO: Initialize to an appropriate value
            List<ArgumentDescription> expected = new List<ArgumentDescription>(); // TODO: Initialize to an appropriate value
            List<ArgumentDescription> actual;
            target.ArgumentDescription = expected;
            actual = target.ArgumentDescription;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Text
        ///</summary>
        [TestMethod()]
        public void TextTest()
        {
            Description target = new Description(); // TODO: Initialize to an appropriate value
            BlockText expected = new BlockText() { Text = new List<string>() }; // TODO: Initialize to an appropriate value
            BlockText actual;
            target.Text = expected;
            actual = target.Text;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
