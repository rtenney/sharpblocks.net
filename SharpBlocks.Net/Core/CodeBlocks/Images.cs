﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SharpBlocks.Net.Core.CodeBlocks
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [DesignerCategory(@"code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class Images //: EntityBase<Images>
    {
        
        private BlockImageIcon _image;

        [XmlElement(Order = 0)]
        public BlockImageIcon Image
        {
            get
            {
                if ((_image == null))
                {
                    _image = new BlockImageIcon();
                }
                return _image;
            }
            set
            {
                if ((_image != null))
                {
                    if ((_image.Equals(value) != true))
                    {
                        _image = value;
                        //OnPropertyChanged("BlockImageIcon");
                    }
                }
                else
                {
                    _image = value;
                    //OnPropertyChanged("BlockImageIcon");
                }
            }
        }
    }
}