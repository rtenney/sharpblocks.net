﻿using SharpBlocks.Net.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using SharpBlocks.Net.Core.Workspace;
using SharpBlocks.Net.Core.Collections;
using SharpBlocks.Net.Core.CodeBlocks;

namespace SharpBlocks.Net.Tests
{
    
    
    /// <summary>
    ///This is a test class for BlockLanguageDefinitionTest and is intended
    ///to contain all BlockLanguageDefinitionTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BlockLanguageDefinitionTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for BlockLanguageDefinition Constructor
        ///</summary>
        [TestMethod()]
        public void BlockLanguageDefinitionConstructorTest()
        {
            BlockLanguageDefinition target = new BlockLanguageDefinition();
            Assert.IsNotNull(target);
            //Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for BlockConnectorShapes
        ///</summary>
        [TestMethod()]
        public void BlockConnectorShapesTest()
        {
            BlockLanguageDefinition target = new BlockLanguageDefinition(); // TODO: Initialize to an appropriate value
            List<BlockConnectorShape> expected = new List<BlockConnectorShape>(); // TODO: Initialize to an appropriate value
            List<BlockConnectorShape> actual;
            target.BlockConnectorShapes = expected;
            actual = target.BlockConnectorShapes;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for BlockDrawerSets
        ///</summary>
        [TestMethod()]
        public void BlockDrawerSetsTest()
        {
            BlockLanguageDefinition target = new BlockLanguageDefinition(); // TODO: Initialize to an appropriate value
            List<BlockDrawerSet> expected = new List<BlockDrawerSet>(); // TODO: Initialize to an appropriate value
            List<BlockDrawerSet> actual;
            target.BlockDrawerSets = expected;
            actual = target.BlockDrawerSets;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for BlockFamilies
        ///</summary>
        [TestMethod()]
        public void BlockFamiliesTest()
        {
            BlockLanguageDefinition target = new BlockLanguageDefinition(); // TODO: Initialize to an appropriate value
            List<BlockFamily> expected = new List<BlockFamily>(); // TODO: Initialize to an appropriate value
            List<BlockFamily> actual;
            target.BlockFamilies = expected;
            actual = target.BlockFamilies;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for BlockGenuses
        ///</summary>
        [TestMethod()]
        public void BlockGenusesTest()
        {
            BlockLanguageDefinition target = new BlockLanguageDefinition(); // TODO: Initialize to an appropriate value
            BlockGenusCollection expected = new BlockGenusCollection(); // TODO: Initialize to an appropriate value
            BlockGenusCollection actual;
            target.BlockGenuses = expected;
            actual = target.BlockGenuses;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for MiniMap
        ///</summary>
        [TestMethod()]
        public void MiniMapTest()
        {
            BlockLanguageDefinition target = new BlockLanguageDefinition(); // TODO: Initialize to an appropriate value
            MiniMap expected = new MiniMap() { EnabledString = "yes" }; // TODO: Initialize to an appropriate value
            MiniMap actual;
            target.MiniMap = expected;
            actual = target.MiniMap;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Pages
        ///</summary>
        [TestMethod()]
        public void PagesTest()
        {
            BlockLanguageDefinition target = new BlockLanguageDefinition(); // TODO: Initialize to an appropriate value
            Pages expected = new Pages() { Page = new List<Page>() }; // TODO: Initialize to an appropriate value
            Pages actual;
            target.Pages = expected;
            actual = target.Pages;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for TrashCan
        ///</summary>
        [TestMethod()]
        public void TrashCanTest()
        {
            BlockLanguageDefinition target = new BlockLanguageDefinition(); // TODO: Initialize to an appropriate value
            TrashCan expected = new TrashCan() { OpenTrashImage = @"C:\Location" }; // TODO: Initialize to an appropriate value
            TrashCan actual;
            target.TrashCan = expected;
            actual = target.TrashCan;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
