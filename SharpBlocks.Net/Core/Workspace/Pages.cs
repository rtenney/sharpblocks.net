﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using SharpBlocks.Net.Core.Utilities;

namespace SharpBlocks.Net.Core.Workspace
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [DesignerCategory(@"code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class Pages //: EntityBase<Pages>
    {
        private List<Page> _page;

        public Pages()
        {
            IsDrawerWithPage = false;
        }

        [XmlIgnore]
        public bool IsDrawerWithPage { get; set; }

        [XmlAttribute("drawer-with-page")]
        [DefaultValue("no")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string IsDrawerWithPageString
        {
            get
            {
                return XmlTypeConverter.ConvertFromBoolean(IsDrawerWithPage);
            }
            set
            {
                if ((IsDrawerWithPage.Equals(value) != true))
                {
                    IsDrawerWithPage = XmlTypeConverter.ConvertToBoolean(value);

                    //OnPropertyChanged("IsDrawerWithPageString");
                }
            }
        }

        [XmlElement("Page", Order = 0)]
        public List<Page> Page
        {
            get
            {
                if ((_page == null))
                {
                    _page = new List<Page>();
                }
                return _page;
            }
            set
            {
                if ((_page != null))
                {
                    if ((_page.Equals(value) != true))
                    {
                        _page = value;

                        //OnPropertyChanged("Page");
                    }
                }
                else
                {
                    _page = value;

                    //OnPropertyChanged("Page");
                }
            }
        }
    }
}