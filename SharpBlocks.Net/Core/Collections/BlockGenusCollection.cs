﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpBlocks.Net.Core.CodeBlocks;

namespace SharpBlocks.Net.Core.Collections
{
    public class BlockGenusCollection : List<BlockGenus>
    {
        public BlockGenusCollection()
            : base(new List<BlockGenus>())
        {

        }

        public new void Add(BlockGenus item)
        {
            base.Add(item);
            BlockGenus.NameToGenus.Add(item.GenusName, item);
        }
    }
}
