﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace SharpBlocks.Net.Core.CodeBlocks
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [DesignerCategory(@"code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class BlockGenuses //: EntityBase<BlockGenuses>
    {
        
        private List<BlockGenus> _blockGenus;

        [XmlElement("BlockGenus", Order = 0)]
        public List<BlockGenus> BlockGenus
        {
            get
            {
                if ((_blockGenus == null))
                {
                    _blockGenus = new List<BlockGenus>();
                }
                return _blockGenus;
            }
            set
            {
                if ((_blockGenus != null))
                {
                    if ((_blockGenus.Equals(value) != true))
                    {
                        _blockGenus = value;
                        //OnPropertyChanged("BlockGenus");
                    }
                }
                else
                {
                    _blockGenus = value;
                    //OnPropertyChanged("BlockGenus");
                }
            }
        }
    }
}