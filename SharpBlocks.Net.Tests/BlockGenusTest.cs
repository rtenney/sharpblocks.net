﻿using SharpBlocks.Net.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using SharpBlocks.Net.Core.Collections;
using SharpBlocks.Net.Core.CodeBlocks;

namespace SharpBlocks.Net.Tests
{
    
    
    /// <summary>
    ///This is a test class for BlockGenusTest and is intended
    ///to contain all BlockGenusTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BlockGenusTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for BlockGenus Constructor
        ///</summary>
        [TestMethod()]
        public void BlockGenusConstructorTest()
        {
            BlockGenus target = new BlockGenus();
            Assert.IsNotNull(target);
            //Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for BlockConnectors
        ///</summary>
        [TestMethod()]
        public void BlockConnectorsTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            List<BlockConnector> expected = new List<BlockConnector>(); // TODO: Initialize to an appropriate value
            List<BlockConnector> actual;
            target.BlockConnectors = expected;
            actual = target.BlockConnectors;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Color
        ///</summary>
        [TestMethod()]
        public void ColorTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            string expected = "255 255 255"; // TODO: Initialize to an appropriate value
            string actual;
            target.Color = expected;
            actual = target.Color;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Description
        ///</summary>
        [TestMethod()]
        public void DescriptionTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            Description expected = new Description() { Text = new BlockText() { Text = new List<string>() }, ArgumentDescription = new List<ArgumentDescription>() }; // TODO: Initialize to an appropriate value
            Description actual;
            target.BlockDescription = expected;
            actual = target.BlockDescription;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for EditableLabel
        ///</summary>
        [TestMethod()]
        public void EditableLabelTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            string expected = "yes"; // TODO: Initialize to an appropriate value
            string actual;
            target.EditableLabelString = expected;
            actual = target.EditableLabelString;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Images
        ///</summary>
        [TestMethod()]
        public void ImagesTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            Images expected = new Images() { Image = new BlockImageIcon() { BlockLocation = ImageLocation.center } }; // TODO: Initialize to an appropriate value
            Images actual;
            target.Images = expected;
            actual = target.Images;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for InitialLabel
        ///</summary>
        [TestMethod()]
        public void InitialLabelTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            string expected = "abc"; // TODO: Initialize to an appropriate value
            string actual;
            target.InitialLabel = expected;
            actual = target.InitialLabel;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsLabelUnique
        ///</summary>
        [TestMethod()]
        public void IsLabelUniqueTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            string expected = "yes"; // TODO: Initialize to an appropriate value
            string actual;
            target.IsLabelUniqueString = expected;
            actual = target.IsLabelUniqueString;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsLabelValue
        ///</summary>
        [TestMethod()]
        public void IsLabelValueTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            string expected = "yes"; // TODO: Initialize to an appropriate value
            string actual;
            target.IsLabelValueString = expected;
            actual = target.IsLabelValueString;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsPageLabelEnabled
        ///</summary>
        [TestMethod()]
        public void IsPageLabelEnabledTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            string expected = "yes"; // TODO: Initialize to an appropriate value
            string actual;
            target.IsPageLabelEnabledString = expected;
            actual = target.IsPageLabelEnabledString;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsStarter
        ///</summary>
        [TestMethod()]
        public void IsStarterTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            string expected = "yes"; // TODO: Initialize to an appropriate value
            string actual;
            target.IsStarterString = expected;
            actual = target.IsStarterString;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsTerminator
        ///</summary>
        [TestMethod()]
        public void IsTerminatorTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            string expected = "yes"; // TODO: Initialize to an appropriate value
            string actual;
            target.IsTerminatorString = expected;
            actual = target.IsTerminatorString;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Kind
        ///</summary>
        [TestMethod()]
        public void KindTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            BlockGenusKinds expected = BlockGenusKinds.command; // TODO: Initialize to an appropriate value
            BlockGenusKinds actual;
            target.Kind = expected;
            actual = target.Kind;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for LabelPrefix
        ///</summary>
        [TestMethod()]
        public void LabelPrefixTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            string expected = "set "; // TODO: Initialize to an appropriate value
            string actual;
            target.LabelPrefix = expected;
            actual = target.LabelPrefix;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for LabelSuffix
        ///</summary>
        [TestMethod()]
        public void LabelSuffixTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            string expected = " of"; // TODO: Initialize to an appropriate value
            string actual;
            target.LabelSuffix = expected;
            actual = target.LabelSuffix;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for LangSpecProperties
        ///</summary>
        [TestMethod()]
        public void LangSpecPropertiesTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            List<LangSpecProperty> expected = new List<LangSpecProperty>(); // TODO: Initialize to an appropriate value
            expected.Add(new LangSpecProperty());
            List<LangSpecProperty> actual;
            target.LangSpecProperties = expected;
            actual = target.LangSpecProperties;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GennusName
        ///</summary>
        [TestMethod()]
        public void NameTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            string expected = "Test"; // TODO: Initialize to an appropriate value
            string actual;
            target.GenusName = expected;
            actual = target.GenusName;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Stubs
        ///</summary>
        [TestMethod()]
        public void StubsTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            StubCollection expected = new StubCollection(); // TODO: Initialize to an appropriate value
            expected.Add(new Stub());
            StubCollection actual;
            target.Stubs = expected;
            actual = target.Stubs;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for BlockGenus Constructor
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SharpBlocks.Net.exe")]
        public void BlockGenusConstructorTest2()
        {
            string blockName = "test";
            BlockGenus block = new BlockGenus();
            block.GenusName = blockName;
            BlockGenus_Accessor.NameToGenus.Add(blockName, block);
            string genusName = blockName; // TODO: Initialize to an appropriate value
            string newGenusName = "new Test"; // TODO: Initialize to an appropriate value
            BlockGenus_Accessor target = new BlockGenus_Accessor(genusName, newGenusName);
            Assert.IsNotNull(target);
            Assert.AreNotSame(target, block);
            //Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for AddToExpandGroup
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SharpBlocks.Net.exe")]
        public void AddToExpandGroupTest()
        {
            List<List<BlockConnector>> groups = new List<List<BlockConnector>>(); // TODO: Initialize to an appropriate value
            BlockConnector socket = new BlockConnector(); // TODO: Initialize to an appropriate value
            socket.ExpandGroup = "test";
            BlockGenus_Accessor.AddToExpandGroup(groups, socket);
            Assert.IsNotNull(BlockGenus_Accessor.GetExpandGroup(groups, "test"));
            //Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for GetExpandGroup
        ///</summary>
        [TestMethod()]
        [DeploymentItem("SharpBlocks.Net.exe")]
        public void GetExpandGroupTest()
        {
            List<List<BlockConnector>> groups = null; // TODO: Initialize to an appropriate value
            string group = string.Empty; // TODO: Initialize to an appropriate value
            List<BlockConnector> expected = null; // TODO: Initialize to an appropriate value
            List<BlockConnector> actual;
            actual = BlockGenus_Accessor.GetExpandGroup(groups, group);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetGenusWithName
        ///</summary>
        [TestMethod()]
        public void GetGenusWithNameTest()
        {
            string genusName = string.Empty; // TODO: Initialize to an appropriate value
            BlockGenus expected = null; // TODO: Initialize to an appropriate value
            BlockGenus actual;
            actual = BlockGenus.GetGenusWithName(genusName);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetProperty
        ///</summary>
        [TestMethod()]
        public void GetPropertyTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            string property = string.Empty; // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            actual = target.GetProperty(property);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ResetAllGenuses
        ///</summary>
        [TestMethod()]
        public void ResetAllGenusesTest()
        {
            BlockGenus.ResetAllGenuses();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for After
        ///</summary>
        [TestMethod()]
        public void AfterTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            BlockConnector expected = null; // TODO: Initialize to an appropriate value
            BlockConnector actual;
            target.After = expected;
            actual = target.After;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for AreSocketsExpandable
        ///</summary>
        [TestMethod()]
        public void AreSocketsExpandableTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.AreSocketsExpandable = expected;
            actual = target.AreSocketsExpandable;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ArgumentDescriptions
        ///</summary>
        [TestMethod()]
        public void ArgumentDescriptionsTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            List<string> actual;
            actual = target.ArgumentDescriptions;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Before
        ///</summary>
        [TestMethod()]
        public void BeforeTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            BlockConnector expected = null; // TODO: Initialize to an appropriate value
            BlockConnector actual;
            target.Before = expected;
            actual = target.Before;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for BlockDescription
        ///</summary>
        [TestMethod()]
        public void BlockDescriptionTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            Description expected = null; // TODO: Initialize to an appropriate value
            Description actual;
            target.BlockDescription = expected;
            actual = target.BlockDescription;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for EditableLabelString
        ///</summary>
        [TestMethod()]
        public void EditableLabelStringTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.EditableLabelString = expected;
            actual = target.EditableLabelString;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ExpandGroups
        ///</summary>
        [TestMethod()]
        public void ExpandGroupsTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            List<List<BlockConnector>> actual;
            actual = target.ExpandGroups;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for FamilyList
        ///</summary>
        [TestMethod()]
        public void FamilyListTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            List<string> expected = null; // TODO: Initialize to an appropriate value
            List<string> actual;
            target.FamilyList = expected;
            actual = target.FamilyList;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GenusName
        ///</summary>
        [TestMethod()]
        public void GenusNameTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.GenusName = expected;
            actual = target.GenusName;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for HasAfterConnector
        ///</summary>
        [TestMethod()]
        public void HasAfterConnectorTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.HasAfterConnector;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for HasBeforeConnector
        ///</summary>
        [TestMethod()]
        public void HasBeforeConnectorTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.HasBeforeConnector;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for HasDefaultArguments
        ///</summary>
        [TestMethod()]
        public void HasDefaultArgumentsTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.HasDefaultArguments = expected;
            actual = target.HasDefaultArguments;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for HasSiblings
        ///</summary>
        [TestMethod()]
        public void HasSiblingsTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.HasSiblings;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for InitialBlockImageMap
        ///</summary>
        [TestMethod()]
        public void InitialBlockImageMapTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            Dictionary<ImageLocation, BlockImageIcon> actual;
            actual = target.InitialBlockImageMap;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for InitialLabel
        ///</summary>
        [TestMethod()]
        public void InitialLabelTest1()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.InitialLabel = expected;
            actual = target.InitialLabel;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsCommandBlock
        ///</summary>
        [TestMethod()]
        public void IsCommandBlockTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.IsCommandBlock;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsDataBlock
        ///</summary>
        [TestMethod()]
        public void IsDataBlockTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.IsDataBlock;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsDeclaration
        ///</summary>
        [TestMethod()]
        public void IsDeclarationTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.IsDeclaration;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsFunctionBlock
        ///</summary>
        [TestMethod()]
        public void IsFunctionBlockTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.IsFunctionBlock;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsInfix
        ///</summary>
        [TestMethod()]
        public void IsInfixTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            //target.IsInfix = expected;
            actual = target.IsInfix;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsLabelEditable
        ///</summary>
        [TestMethod()]
        public void IsLabelEditableTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.IsLabelEditable = expected;
            actual = target.IsLabelEditable;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsLabelUniqueString
        ///</summary>
        [TestMethod()]
        public void IsLabelUniqueStringTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.IsLabelUniqueString = expected;
            actual = target.IsLabelUniqueString;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsLabelValueString
        ///</summary>
        [TestMethod()]
        public void IsLabelValueStringTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.IsLabelValueString = expected;
            actual = target.IsLabelValueString;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsListRelated
        ///</summary>
        [TestMethod()]
        public void IsListRelatedTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.IsListRelated;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsPageLabelEnabledString
        ///</summary>
        [TestMethod()]
        public void IsPageLabelEnabledStringTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.IsPageLabelEnabledString = expected;
            actual = target.IsPageLabelEnabledString;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsProcedureDeclBlock
        ///</summary>
        [TestMethod()]
        public void IsProcedureDeclBlockTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.IsProcedureDeclBlock;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsProcedureParamBlock
        ///</summary>
        [TestMethod()]
        public void IsProcedureParamBlockTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.IsProcedureParamBlock;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsStarterString
        ///</summary>
        [TestMethod()]
        public void IsStarterStringTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.IsStarterString = expected;
            actual = target.IsStarterString;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsTerminatorString
        ///</summary>
        [TestMethod()]
        public void IsTerminatorStringTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.IsTerminatorString = expected;
            actual = target.IsTerminatorString;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsVariableDeclBlock
        ///</summary>
        [TestMethod()]
        public void IsVariableDeclBlockTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.IsVariableDeclBlock;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for NameToGenus
        ///</summary>
        [TestMethod()]
        public void NameToGenusTest()
        {
            Dictionary<string, BlockGenus> actual;
            actual = BlockGenus.NameToGenus;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Plug
        ///</summary>
        [TestMethod()]
        public void PlugTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            BlockConnector expected = null; // TODO: Initialize to an appropriate value
            BlockConnector actual;
            //target.Plug = expected;
            actual = target.Plug;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Properties
        ///</summary>
        [TestMethod()]
        public void PropertiesTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            Dictionary<string, string> expected = null; // TODO: Initialize to an appropriate value
            Dictionary<string, string> actual;
            target.Properties = expected;
            actual = target.Properties;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Sockets
        ///</summary>
        [TestMethod()]
        public void SocketsTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            List<BlockConnector> expected = null; // TODO: Initialize to an appropriate value
            List<BlockConnector> actual;
            //target.Sockets = expected;
            actual = target.Sockets;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for StubList
        ///</summary>
        [TestMethod()]
        public void StubListTest()
        {
            BlockGenus target = new BlockGenus(); // TODO: Initialize to an appropriate value
            List<string> expected = null; // TODO: Initialize to an appropriate value
            List<string> actual;
            target.StubList = expected;
            actual = target.StubList;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
