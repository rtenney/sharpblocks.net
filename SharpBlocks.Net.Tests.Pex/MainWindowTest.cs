using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Pex.Framework.Generated;
using Microsoft.Pex.Framework.Validation;
// <copyright file="MainWindowTest.cs">Copyright �  2013</copyright>
namespace SharpBlocks.Net
{
    /// <summary>This class contains parameterized unit tests for MainWindow</summary>
    [global::Microsoft.Pex.Framework.PexClass(typeof(global::SharpBlocks.Net.MainWindow))]
    [global::Microsoft.Pex.Framework.Validation.PexAllowedExceptionFromTypeUnderTest(typeof(global::System.InvalidOperationException))]
    [global::Microsoft.Pex.Framework.Validation.PexAllowedExceptionFromTypeUnderTest(typeof(global::System.ArgumentException), AcceptExceptionSubtypes = true)]
    [global::Microsoft.VisualStudio.TestTools.UnitTesting.TestClass]
    public partial class MainWindowTest
    {
        /// <summary>Test stub for .ctor()</summary>
        [global::Microsoft.Pex.Framework.PexMethod, PexAllowedException(typeof(InvalidOperationException))]
        public global::SharpBlocks.Net.MainWindow Constructor()
        {
            global::SharpBlocks.Net.MainWindow target
               = new global::SharpBlocks.Net.MainWindow();
            return target;
            // TODO: add assertions to method MainWindowTest.Constructor()
        }
        [TestMethod]
        public void ConstructorThrowsInvalidOperationException470()
        {
            MainWindow mainWindow;
            mainWindow = this.Constructor();
        }
    }
}
