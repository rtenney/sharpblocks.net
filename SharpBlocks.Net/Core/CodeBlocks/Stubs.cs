﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace SharpBlocks.Net.Core.CodeBlocks
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [DesignerCategory(@"code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class Stubs
    {
        
        private List<Stub> _stub;

        [XmlElement("Stub", Order = 0)]
        public List<Stub> Stub
        {
            get
            {
                if ((_stub == null))
                {
                    _stub = new List<Stub>();
                }
                return _stub;
            }
            set
            {
                if ((_stub != null))
                {
                    if ((_stub.Equals(value) != true))
                    {
                        _stub = value;
                    }
                }
                else
                {
                    _stub = value;
                }
            }
        }
    }
}