﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using SharpBlocks.Net.Core.Utilities;
using SharpBlocks.Net.Core.CodeBlocks;

namespace SharpBlocks.Net.Core.Workspace
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [DesignerCategory(@"code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class BlockDrawer //: EntityBase<BlockDrawer>
    {
        private string _buttoncColor;

        private List<object> _items;

        private string _name;

        private BlockDrawerType _type;

        public BlockDrawer()
        {
            _type = BlockDrawerType.@default;
            IsOpen = false;
        }

        [XmlAttribute("button-color")]
        public string ButtonColor
        {
            get
            {
                return _buttoncColor;
            }
            set
            {
                if ((_buttoncColor != null))
                {
                    if ((_buttoncColor.Equals(value) != true))
                    {
                        _buttoncColor = value;

                        //OnPropertyChanged("ButtonColor");
                    }
                }
                else
                {
                    _buttoncColor = value;

                    //OnPropertyChanged("buttoncolor");
                }
            }
        }

        [XmlIgnore]
        public bool IsOpen { get; set; }

        [XmlAttribute("is-open")]
        [DefaultValue("no")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string IsOpenString
        {
            get
            {
                return XmlTypeConverter.ConvertFromBoolean(IsOpen);
            }
            set
            {
                if ((IsOpen.Equals(value) != true))
                {
                    IsOpen = XmlTypeConverter.ConvertToBoolean(value);

                    //OnPropertyChanged("IsOpenString");
                }
            }
        }

        [XmlElement("BlockGenusMember", typeof(string), Order = 0)]
        [XmlElement("NextLine", typeof(NextLine), Order = 0)]
        [XmlElement("Separator", typeof(Separator), Order = 0)]
        public List<object> Items
        {
            get
            {
                if ((_items == null))
                {
                    _items = new List<object>();
                }
                return _items;
            }
            set
            {
                if ((_items != null))
                {
                    if ((_items.Equals(value) != true))
                    {
                        _items = value;

                        //OnPropertyChanged("Items");
                    }
                }
                else
                {
                    _items = value;

                    //OnPropertyChanged("Items");
                }
            }
        }

        [XmlAttribute("name")]
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if ((_name != null))
                {
                    if ((_name.Equals(value) != true))
                    {
                        _name = value;

                        //OnPropertyChanged("GennusName");
                    }
                }
                else
                {
                    _name = value;

                    //OnPropertyChanged("GennusName");
                }
            }
        }

        [XmlAttribute("type")]
        [DefaultValue(BlockDrawerType.@default)]
        public BlockDrawerType Type
        {
            get
            {
                return _type;
            }
            set
            {
                if ((_type.Equals(value) != true))
                {
                    _type = value;

                    //OnPropertyChanged("Type");
                }
            }
        }
    }
}