// <copyright file="ExtensionMethodsTest.cs">Copyright �  2013</copyright>
namespace SharpBlocks.Net
{
    /// <summary>This class contains parameterized unit tests for ExtensionMethods</summary>
    [global::Microsoft.Pex.Framework.PexClass(typeof(global::SharpBlocks.Net.Core.Utilities.ExtensionMethods))]
    [global::Microsoft.Pex.Framework.Validation.PexAllowedExceptionFromTypeUnderTest(typeof(global::System.InvalidOperationException))]
    [global::Microsoft.Pex.Framework.Validation.PexAllowedExceptionFromTypeUnderTest(typeof(global::System.ArgumentException), AcceptExceptionSubtypes = true)]
    [global::Microsoft.VisualStudio.TestTools.UnitTesting.TestClass]
    public partial class ExtensionMethodsTest
    {
        /// <summary>Test stub for Deserialize(String)</summary>
        [global::Microsoft.Pex.Framework.PexGenericArguments(typeof(int))]
        [global::Microsoft.Pex.Framework.PexMethod]
        public T Deserialize<T>(string serializedData)
        {
            T result
               = global::SharpBlocks.Net.Core.Utilities.ExtensionMethods.Deserialize<T>(serializedData);
            return result;
            // TODO: add assertions to method ExtensionMethodsTest.Deserialize(String)
        }

        /// <summary>Test stub for Serialize(Object)</summary>
        [global::Microsoft.Pex.Framework.PexMethod]
        public string Serialize(object obj)
        {
            string result = global::SharpBlocks.Net.Core.Utilities.ExtensionMethods.Serialize(obj);
            return result;
            // TODO: add assertions to method ExtensionMethodsTest.Serialize(Object)
        }
    }
}
