﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace SharpBlocks.Net.Core.CodeBlocks
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [DesignerCategory(@"code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class BlockFamilies //: EntityBase<BlockFamilies>
    {
        
        private List<string> _blockFamily;

        [XmlArray(Order = 0)]
        [XmlArrayItem("FamilyMember", typeof (string), IsNullable = false)]
        public List<string> BlockFamily
        {
            get
            {
                if ((_blockFamily == null))
                {
                    _blockFamily = new List<string>();
                }
                return _blockFamily;
            }
            set
            {
                if ((_blockFamily != null))
                {
                    if ((_blockFamily.Equals(value) != true))
                    {
                        _blockFamily = value;
                        //OnPropertyChanged("BlockFamily");
                    }
                }
                else
                {
                    _blockFamily = value;
                    //OnPropertyChanged("BlockFamily");
                }
            }
        }
    }
}