﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpBlocks.Net.Core.CodeBlocks;

namespace SharpBlocks.Net.Core.Interfaces
{
    public interface ILinkRule
    {

        bool CanLink(Block block1, Block block2, BlockConnector socket1, BlockConnector socket2);

        bool IsMandatory();

    }
}
