﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace SharpBlocks.Net.Core.Workspace
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [DesignerCategory(@"code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class Page //: EntityBase<Page>
    {

        private List<string> _pageDrawers;

        private string _pageColor;
        private string _pageDrawer;
        private string _pageName;

        private string _pageShape;
        private int _pageWidth;

        [XmlArray(ElementName = "PageDrawer", Order = 0)]
        [XmlArrayItem("BlockGenusMember", IsNullable = false)]
        public List<string> PageDrawers
        {
            get
            {
                if ((_pageDrawers == null))
                {
                    _pageDrawers = new List<string>();
                }
                return _pageDrawers;
            }
            set
            {
                if ((_pageDrawers != null))
                {
                    if ((_pageDrawers.Equals(value) != true))
                    {
                        _pageDrawers = value;
                        //OnPropertyChanged("PageDrawers");
                    }
                }
                else
                {
                    _pageDrawers = value;
                    //OnPropertyChanged("PageDrawers");
                }
            }
        }

        [XmlAttribute("page-name")]
        public string PageName
        {
            get { return _pageName; }
            set
            {
                if ((_pageName != null))
                {
                    if ((_pageName.Equals(value) != true))
                    {
                        _pageName = value;
                        //OnPropertyChanged("PageName");
                    }
                }
                else
                {
                    _pageName = value;
                    //OnPropertyChanged("PageName");
                }
            }
        }

        [XmlAttribute("page-width")]
        public int PageWidth
        {
            get { return _pageWidth; }
            set
            {
                if ((_pageWidth != null))
                {
                    if ((_pageWidth.Equals(value) != true))
                    {
                        _pageWidth = value;
                        //OnPropertyChanged("PageWidth");
                    }
                }
                else
                {
                    _pageWidth = value;
                    //OnPropertyChanged("PageWidth");
                }
            }
        }

        [XmlAttribute("page-drawer")]
        public string PageDrawer
        {
            get { return _pageDrawer; }
            set
            {
                if ((_pageDrawer != null))
                {
                    if ((_pageDrawer.Equals(value) != true))
                    {
                        _pageDrawer = value;
                        //OnPropertyChanged("PageDrawer");
                    }
                }
                else
                {
                    _pageDrawer = value;
                    //OnPropertyChanged("PageDrawer");
                }
            }
        }

        [XmlAttribute("page-color")]
        public string PageColor
        {
            get { return _pageColor; }
            set
            {
                if ((_pageColor != null))
                {
                    if ((_pageColor.Equals(value) != true))
                    {
                        _pageColor = value;
                        //OnPropertyChanged("PageColor");
                    }
                }
                else
                {
                    _pageColor = value;
                    //OnPropertyChanged("PageColor");
                }
            }
        }

        [XmlAttribute("page-shape")]
        public string PageShape
        {
            get { return _pageShape; }
            set
            {
                if ((_pageShape != null))
                {
                    if ((_pageShape.Equals(value) != true))
                    {
                        _pageShape = value;
                        //OnPropertyChanged("PageShape");
                    }
                }
                else
                {
                    _pageShape = value;
                    //OnPropertyChanged("PageShape");
                }
            }
        }
    }
}