﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace SharpBlocks.Net.Core.CodeBlocks
{
    public class Block
    {
        public const long NULL = -1;

        static Block()
        {
            ALL_BLOCKS = new Dictionary<long, Block>();
            NEXT_ID = 1;
        }

        public Block()
        {
            Properties = new Dictionary<string, string>();
            LinkToStubs = true;
            IsBad = false;
            HasFocus = false;
            PageLabel = null;
        }

        public Block(string genusName) : this(genusName, BlockGenus.GetGenusWithName(genusName).InitialLabel) { }

        public Block(string genusName, bool linkToStubs) : this(genusName, BlockGenus.GetGenusWithName(genusName).InitialLabel, linkToStubs) { }

        public Block(string genusName, string lable, bool linkToStubs)
            : this(NEXT_ID, genusName, lable, linkToStubs)
        {
            NEXT_ID++;

            while (ALL_BLOCKS.ContainsKey(NEXT_ID))
                NEXT_ID++;
        }

        public Block(string genusName, string label) :this(NEXT_ID, genusName, label, true)
        {
            NEXT_ID++;

            while (ALL_BLOCKS.ContainsKey(NEXT_ID))
                NEXT_ID++;
        }

        protected Block(long id, string genusName, string label, bool linkToStubs) :this()
        {
            Debug.Assert(!ALL_BLOCKS.ContainsKey(id), String.Format("Block id: {0} already exists!  BlockGenus {1} label: {2}", id, genusName, label));

            if (ALL_BLOCKS.ContainsKey(id))
            {
                Block dup = ALL_BLOCKS[id];
                Console.WriteLine(String.Format("pre-existing block is: {0} with genus {1} and label {2}", dup, dup.GenusName, dup.BlockLabel));
            }
            this.BlockId = id;

            //if this assigned id value equals the next id to automatically assign 
            // a new block, increment the next id value by 1
            if (id == NEXT_ID)
                NEXT_ID++;

            Sockets = new List<BlockConnector>();
            ArgumentDescriptions = new List<string>();
            //copy connectors from BlockGenus
            try
            {
                BlockGenus genus = BlockGenus.GetGenusWithName(genusName);
                if (genus == null)
                    throw new Exception(String.Format("genusName: {0} does not exist.", genusName));

                //copy the block connectors from block genus
                //Iterable<BlockConnector> iter = genus.getInitSockets();
                foreach (BlockConnector con in genus.Sockets)
                {
                    Sockets.Add(new BlockConnector(con));
                }

                if (genus.Plug != null)
                    Plug = new BlockConnector(genus.Plug);

                if (genus.Before != null)
                    Before = new BlockConnector(genus.Before);

                if (genus.After != null)
                    After = new BlockConnector(genus.After);

                this.GenusName = genusName;

                this.BlockLabel = label;

                //Iterable<String> arguumentIter = genus.getInitialArgumentDescriptions();
                foreach (string arg in genus.ArgumentDescriptions)
                {
                    ArgumentDescriptions.Add(arg.Trim());
                }

                this.ExpandGroups = new List<List<BlockConnector>>(genus.ExpandGroups);

                //add to ALL_BLOCKS
                //warning: publishing this block before constructor finishes has the 
                //potential to cause some problems such as data races
                //other threads could access this block from getBlock()
                ALL_BLOCKS.Add(this.BlockId, this);

                //add itself to stubs hashmap
                //however factory blocks will have entries in hashmap...
                //if(linkToStubs && this.HasStubs){
                //    BlockStub.putNewParentInStubMap(this.blockID);
                //}

            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
        }


        public static Dictionary<long, Block> ALL_BLOCKS { get; set; }

        public static long NEXT_ID { get; set; }

        public BlockConnector After { get; set; }

        public List<String> ArgumentDescriptions { get; set; }

        public string BadMessage { get; set; }

        public BlockConnector Before { get; set; }

        public long BlockId { get; set; }

        public long ConnectionBlockId { get; set; }

        public List<List<BlockConnector>> ExpandGroups { get; set; }

        public string GenusName { get; set; }

        public bool HasFocus { get; set; }

        public bool IsBad { get; set; }

        public string BlockLabel { get; set; }

        public bool LinkToStubs { get; set; }

        public string PageLabel { get; set; }

        public BlockConnector Plug { get; set; }

        public Dictionary<string, string> Properties { get; set; }

        public List<BlockConnector> Sockets { get; set; }

        public bool HasStubs { get; set; }
    }
}