﻿using SharpBlocks.Net.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using SharpBlocks.Net.Core.CodeBlocks;
using SharpBlocks.Net.Core.Workspace;

namespace SharpBlocks.Net.Tests
{
    
    
    /// <summary>
    ///This is a test class for BlockDrawerTest and is intended
    ///to contain all BlockDrawerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BlockDrawerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for BlockDrawer Constructor
        ///</summary>
        [TestMethod()]
        public void BlockDrawerConstructorTest()
        {
            BlockDrawer target = new BlockDrawer();
            Assert.IsNotNull(target);
            //Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for ButtonColor
        ///</summary>
        [TestMethod()]
        public void ButtonColorTest()
        {
            BlockDrawer target = new BlockDrawer(); // TODO: Initialize to an appropriate value
            string expected = "255 255 255"; // TODO: Initialize to an appropriate value
            string actual;
            target.ButtonColor = expected;
            actual = target.ButtonColor;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsOpen
        ///</summary>
        [TestMethod()]
        public void IsOpenTest()
        {
            BlockDrawer target = new BlockDrawer(); // TODO: Initialize to an appropriate value
            string expected = "yes"; // TODO: Initialize to an appropriate value
            string actual;
            target.IsOpenString = expected;
            actual = target.IsOpenString;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Items
        ///</summary>
        [TestMethod()]
        public void ItemsTest()
        {
            BlockDrawer target = new BlockDrawer(); // TODO: Initialize to an appropriate value
            List<object> expected = new List<object>(); // TODO: Initialize to an appropriate value
            expected.Add(new Separator());
            List<object> actual;
            target.Items = expected;
            actual = target.Items;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GennusName
        ///</summary>
        [TestMethod()]
        public void NameTest()
        {
            BlockDrawer target = new BlockDrawer(); // TODO: Initialize to an appropriate value
            string expected = "test"; // TODO: Initialize to an appropriate value
            string actual;
            target.Name = expected;
            actual = target.Name;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Type
        ///</summary>
        [TestMethod()]
        public void TypeTest()
        {
            BlockDrawer target = new BlockDrawer(); // TODO: Initialize to an appropriate value
            BlockDrawerType expected = new BlockDrawerType(); // TODO: Initialize to an appropriate value
            BlockDrawerType actual;
            target.Type = expected;
            actual = target.Type;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
