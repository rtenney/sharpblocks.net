﻿using SharpBlocks.Net.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SharpBlocks.Net.Core.CodeBlocks;

namespace SharpBlocks.Net.Tests
{
    
    
    /// <summary>
    ///This is a test class for ImageTest and is intended
    ///to contain all ImageTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ImageTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for BlockImageIcon Constructor
        ///</summary>
        [TestMethod()]
        public void ImageConstructorTest()
        {
            BlockImageIcon target = new BlockImageIcon();
            Assert.IsNotNull(target);
            //Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for BlockLocation
        ///</summary>
        [TestMethod()]
        public void BlockLocationTest()
        {
            BlockImageIcon target = new BlockImageIcon(); // TODO: Initialize to an appropriate value
            ImageLocation expected = ImageLocation.center;
            ImageLocation actual;
            target.BlockLocation = expected;
            actual = target.BlockLocation;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for FileLocation
        ///</summary>
        [TestMethod()]
        public void FileLocationTest()
        {
            BlockImageIcon target = new BlockImageIcon(); // TODO: Initialize to an appropriate value
            string expected = @"C:\FileLocation"; // TODO: Initialize to an appropriate value
            string actual;
            target.FileLocation = expected;
            actual = target.FileLocation;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Height
        ///</summary>
        [TestMethod()]
        public void HeightTest()
        {
            BlockImageIcon target = new BlockImageIcon(); // TODO: Initialize to an appropriate value
            int expected = 15; // TODO: Initialize to an appropriate value
            int actual;
            target.Height = expected;
            actual = target.Height;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsImageEditable
        ///</summary>
        [TestMethod()]
        public void IsImageEditableTest()
        {
            BlockImageIcon target = new BlockImageIcon(); // TODO: Initialize to an appropriate value
            string expected = "yes"; // TODO: Initialize to an appropriate value
            string actual;
            target.IsImageEditableString = expected;
            actual = target.IsImageEditableString;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Width
        ///</summary>
        [TestMethod()]
        public void WidthTest()
        {
            BlockImageIcon target = new BlockImageIcon(); // TODO: Initialize to an appropriate value
            int expected = 15; // TODO: Initialize to an appropriate value
            int actual;
            target.Width = expected;
            actual = target.Width;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for WrapText
        ///</summary>
        [TestMethod()]
        public void WrapTextTest()
        {
            BlockImageIcon target = new BlockImageIcon(); // TODO: Initialize to an appropriate value
            string expected = "yes"; // TODO: Initialize to an appropriate value
            string actual;
            target.WrapTextString = expected;
            actual = target.WrapTextString;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
