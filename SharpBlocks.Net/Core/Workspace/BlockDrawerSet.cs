﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using SharpBlocks.Net.Core.Utilities;

namespace SharpBlocks.Net.Core.Workspace
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [DesignerCategory(@"code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class BlockDrawerSet //: EntityBase<BlockDrawerSet>
    {
        private List<BlockDrawer> _blockDrawer;

        private BlockDrawerSetLocation _location;

        private string _name;

        private BlockDrawerSetType _type;

        private int _width;

        public BlockDrawerSet()
        {
            _type = BlockDrawerSetType.bar;
            _location = BlockDrawerSetLocation.west;
            HasWindowPerDrawer = true;
            IsDrawerDraggable = true;
        }

        [XmlElement("BlockDrawer", Order = 0)]
        public List<BlockDrawer> BlockDrawer
        {
            get
            {
                if ((_blockDrawer == null))
                {
                    _blockDrawer = new List<BlockDrawer>();
                }
                return _blockDrawer;
            }
            set
            {
                if ((_blockDrawer != null))
                {
                    if ((_blockDrawer.Equals(value) != true))
                    {
                        _blockDrawer = value;

                        //OnPropertyChanged("BlockDrawer");
                    }
                }
                else
                {
                    _blockDrawer = value;

                    //OnPropertyChanged("BlockDrawer");
                }
            }
        }

        [XmlIgnore]
        public bool HasWindowPerDrawer { get; set; }

        [XmlAttribute("window-per-drawer")]
        [DefaultValue("yes")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string HasWindowPerDrawerString
        {
            get
            {
                return XmlTypeConverter.ConvertFromBoolean(HasWindowPerDrawer);
            }
            set
            {
                if ((HasWindowPerDrawer.Equals(value) != true))
                {
                    HasWindowPerDrawer = XmlTypeConverter.ConvertToBoolean(value);

                    //OnPropertyChanged("HasWindowPerDrawerString");
                }
            }
        }

        [XmlIgnore]
        public bool IsDrawerDraggable { get; set; }

        [XmlAttribute("drawer-draggable")]
        [DefaultValue("yes")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string IsDrawerDraggableString
        {
            get
            {
                return XmlTypeConverter.ConvertFromBoolean(IsDrawerDraggable);
            }
            set
            {
                if ((IsDrawerDraggable.Equals(value) != true))
                {
                    IsDrawerDraggable = XmlTypeConverter.ConvertToBoolean(value);

                    //OnPropertyChanged("IsDrawerDraggableString");
                }
            }
        }

        [XmlAttribute("location")]
        [DefaultValue(BlockDrawerSetLocation.west)]
        public BlockDrawerSetLocation Location
        {
            get
            {
                return _location;
            }
            set
            {
                if ((_location.Equals(value) != true))
                {
                    _location = value;

                    //OnPropertyChanged("Location");
                }
            }
        }

        [XmlAttribute("name")]
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if ((_name != null))
                {
                    if ((_name.Equals(value) != true))
                    {
                        _name = value;

                        //OnPropertyChanged("GennusName");
                    }
                }
                else
                {
                    _name = value;

                    //OnPropertyChanged("GennusName");
                }
            }
        }

        [XmlAttribute("type")]
        [DefaultValue(BlockDrawerSetType.bar)]
        public BlockDrawerSetType Type
        {
            get
            {
                return _type;
            }
            set
            {
                if ((_type.Equals(value) != true))
                {
                    _type = value;

                    //OnPropertyChanged("Type");
                }
            }
        }

        [XmlAttribute("width")]
        public int Width
        {
            get
            {
                return _width;
            }
            set
            {
                if ((_width != null))
                {
                    if ((_width.Equals(value) != true))
                    {
                        _width = value;

                        //OnPropertyChanged("Width");
                    }
                }
                else
                {
                    _width = value;

                    //OnPropertyChanged("Width");
                }
            }
        }
    }
}