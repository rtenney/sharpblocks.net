// <copyright file="ViewModelLocatorTest.cs">Copyright �  2013</copyright>
namespace SharpBlocks.Net.ViewModel
{
    /// <summary>This class contains parameterized unit tests for ViewModelLocator</summary>
    [global::Microsoft.Pex.Framework.PexClass(typeof(global::SharpBlocks.Net.ViewModel.ViewModelLocator))]
    [global::Microsoft.Pex.Framework.Validation.PexAllowedExceptionFromTypeUnderTest(typeof(global::System.InvalidOperationException))]
    [global::Microsoft.Pex.Framework.Validation.PexAllowedExceptionFromTypeUnderTest(typeof(global::System.ArgumentException), AcceptExceptionSubtypes = true)]
    [global::Microsoft.VisualStudio.TestTools.UnitTesting.TestClass]
    public partial class ViewModelLocatorTest
    {
        /// <summary>Test stub for Cleanup()</summary>
        [global::Microsoft.Pex.Framework.PexMethod]
        public void Cleanup()
        {
            global::SharpBlocks.Net.ViewModel.ViewModelLocator.Cleanup();
            // TODO: add assertions to method ViewModelLocatorTest.Cleanup()
        }

        /// <summary>Test stub for get_Main()</summary>
        [global::Microsoft.Pex.Framework.PexMethod]
        public global::SharpBlocks.Net.ViewModel.MainViewModel MainGet([global::Microsoft.Pex.Framework.PexAssumeUnderTest]global::SharpBlocks.Net.ViewModel.ViewModelLocator target)
        {
            global::SharpBlocks.Net.ViewModel.MainViewModel result = target.Main;
            return result;
            // TODO: add assertions to method ViewModelLocatorTest.MainGet(ViewModelLocator)
        }
    }
}
