﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SharpBlocks.Net.UserControls
{
    /// <summary>
    /// Interaction logic for QueryField.xaml
    /// </summary>
    public partial class QueryField : UserControl
    {
        public QueryField()
        {
            InitializeComponent();
        }

        /// <summary>
        /// The <see cref="Text" /> dependency property's name.
        /// </summary>
        public const string TextPropertyName = "Text";

        /// <summary>
        /// Gets or sets the value of the <see cref="Text" />
        /// property. This is a dependency property.
        /// </summary>
        public string Text
        {
            get
            {
                return (string)GetValue(TextProperty);
            }
            set
            {
                SetValue(TextProperty, value);
            }
        }

        /// <summary>
        /// Identifies the <see cref="Text" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty TextProperty = DependencyProperty.Register(
            TextPropertyName,
            typeof(string),
            typeof(QueryField),
            new UIPropertyMetadata(string.Empty));
    }
}
