// <copyright file="XmlTypeConverterTest.cs">Copyright �  2013</copyright>
namespace SharpBlocks.Net.Core
{
    /// <summary>This class contains parameterized unit tests for XmlTypeConverter</summary>
    [global::Microsoft.Pex.Framework.PexClass(typeof(global::SharpBlocks.Net.Core.Utilities.XmlTypeConverter))]
    [global::Microsoft.Pex.Framework.Validation.PexAllowedExceptionFromTypeUnderTest(typeof(global::System.InvalidOperationException))]
    [global::Microsoft.Pex.Framework.Validation.PexAllowedExceptionFromTypeUnderTest(typeof(global::System.ArgumentException), AcceptExceptionSubtypes = true)]
    [global::Microsoft.VisualStudio.TestTools.UnitTesting.TestClass]
    public partial class XmlTypeConverterTest
    {
        /// <summary>Test stub for ConvertFromBoolean(Boolean)</summary>
        [global::Microsoft.Pex.Framework.PexMethod]
        public string ConvertFromBoolean(bool value)
        {
            string result
               = global::SharpBlocks.Net.Core.Utilities.XmlTypeConverter.ConvertFromBoolean(value);
            return result;
            // TODO: add assertions to method XmlTypeConverterTest.ConvertFromBoolean(Boolean)
        }

        /// <summary>Test stub for ConvertToBoolean(String)</summary>
        [global::Microsoft.Pex.Framework.PexMethod]
        public bool ConvertToBoolean(string value)
        {
            bool result
               = global::SharpBlocks.Net.Core.Utilities.XmlTypeConverter.ConvertToBoolean(value);
            return result;
            // TODO: add assertions to method XmlTypeConverterTest.ConvertToBoolean(String)
        }
    }
}
