// <copyright file="DataItemTest.cs">Copyright �  2013</copyright>
namespace SharpBlocks.Net.Model
{
    /// <summary>This class contains parameterized unit tests for DataItem</summary>
    [global::Microsoft.Pex.Framework.PexClass(typeof(global::SharpBlocks.Net.Model.DataItem))]
    [global::Microsoft.Pex.Framework.Validation.PexAllowedExceptionFromTypeUnderTest(typeof(global::System.InvalidOperationException))]
    [global::Microsoft.Pex.Framework.Validation.PexAllowedExceptionFromTypeUnderTest(typeof(global::System.ArgumentException), AcceptExceptionSubtypes = true)]
    [global::Microsoft.VisualStudio.TestTools.UnitTesting.TestClass]
    public partial class DataItemTest
    {
        /// <summary>Test stub for .ctor(String)</summary>
        [global::Microsoft.Pex.Framework.PexMethod]
        public global::SharpBlocks.Net.Model.DataItem Constructor(string title)
        {
            global::SharpBlocks.Net.Model.DataItem target
               = new global::SharpBlocks.Net.Model.DataItem(title);
            return target;
            // TODO: add assertions to method DataItemTest.Constructor(String)
        }
    }
}
