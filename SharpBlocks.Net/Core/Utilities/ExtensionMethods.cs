﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Schema;

namespace SharpBlocks.Net.Core.Utilities
{
    public static class ExtensionMethods
    {
        /// <summary>
        /// Serializes the specified obj.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <returns>A string representing serialized data</returns>
        public static string Serialize(this object obj)
        {
            // <pex>
            if (obj == (object)null)
                throw new ArgumentNullException("obj");
            // </pex>
            //Check is object is serializable before trying to serialize
            if (obj.GetType().IsSerializable)
            {
                using (var stream = new MemoryStream())
                {
                    var serializer = new XmlSerializer(obj.GetType());
                    serializer.Serialize(stream, obj);
                    var bytes = new byte[stream.Length];
                    stream.Position = 0;
                    stream.Read(bytes, 0, bytes.Length);

                    return Encoding.UTF8.GetString(bytes);
                }
            }
            throw new NotSupportedException(string.Format("{0} is not serializable.", obj.GetType()));
        }

        /// <summary>
        /// Deserializes the specified serialized data.
        /// </summary>
        /// <param name="serializedData">The serialized data.</param>
        /// <returns></returns>
        [STAThread]
        public static T Deserialize<T>(this string serializedData)
        {
            // <pex>
            if (serializedData == (string)null)
                throw new ArgumentNullException("serializedData");
            // </pex>
            var serializer = new XmlSerializer(typeof(T));
            var validator = new XmlReaderSettings();
            validator.ValidationType = ValidationType.DTD;
            validator.DtdProcessing = DtdProcessing.Parse;
            validator.ValidationEventHandler += new System.Xml.Schema.ValidationEventHandler(validator_ValidationEventHandler);
            var reader = XmlReader.Create(new StringReader(serializedData), validator);
            try
            {
                return (T)serializer.Deserialize(reader);
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine("Error in deserialization: {0}", e.Message);
                throw new InvalidOperationException(e.Message);
            }
        }

        static void validator_ValidationEventHandler(object sender, System.Xml.Schema.ValidationEventArgs e)
        {
            if (e.Severity == XmlSeverityType.Warning)
                Console.WriteLine("\tWarning: Matching schema not found.  No validation occurred." + e.Message);
            else
                Console.WriteLine("\tValidation error: " + e.Message);
        }
    }
}
