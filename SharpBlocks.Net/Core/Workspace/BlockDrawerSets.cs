﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SharpBlocks.Net.Core.Workspace
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [DesignerCategory(@"code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class BlockDrawerSets //: EntityBase<BlockDrawerSets>
    {
        
        private List<BlockDrawerSet> _blockDrawerSet;

        [XmlElement("BlockDrawerSet", Order = 0)]
        public List<BlockDrawerSet> BlockDrawerSet
        {
            get
            {
                if ((_blockDrawerSet == null))
                {
                    _blockDrawerSet = new List<BlockDrawerSet>();
                }
                return _blockDrawerSet;
            }
            set
            {
                if ((_blockDrawerSet != null))
                {
                    if ((_blockDrawerSet.Equals(value) != true))
                    {
                        _blockDrawerSet = value;
                        //OnPropertyChanged("BlockDrawerSet");
                    }
                }
                else
                {
                    _blockDrawerSet = value;
                    //OnPropertyChanged("BlockDrawerSet");
                }
            }
        }
    }
}