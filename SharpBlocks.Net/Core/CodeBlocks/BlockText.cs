﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SharpBlocks.Net.Core.CodeBlocks
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [DesignerCategory(@"code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(ElementName = "text", Namespace = "", IsNullable = false)]
    public class BlockText
    {
        private object[] _items;
        private ItemsChoiceType[] _itemsElementName;
        private List<string> _text;

        [XmlElement("arg", typeof (Arguments), Order = 0)]
        [XmlElement("br", typeof (string), Order = 0)]
        [XmlElement("em", typeof (string), Order = 0)]
        [XmlElement("i", typeof (string), Order = 0)]
        [XmlElement("note", typeof (Note), Order = 0)]
        [XmlChoiceIdentifier("ItemsElementName")]
        public object[] Items
        {
            get
            {
                //if ((_items == null))
                //{
                //    _items = new List<object>();
                //}
                return _items;
            }
            set
            {
                if ((_items != null))
                {
                    if ((_items.Equals(value) != true))
                    {
                        _items = value;
                    }
                }
                else
                {
                    _items = value;
                }
            }
        }

        //[XmlElement("ItemsElementName", Order = 1)]
        [XmlIgnore]
        public ItemsChoiceType[] ItemsElementName
        {
            get
            {
                //if ((_itemsElementName == null))
                //{
                //    _itemsElementName = new List<ItemsChoiceType>();
                //}
                return _itemsElementName;
            }
            set
            {
                if ((_itemsElementName != null))
                {
                    if ((_itemsElementName.Equals(value) != true))
                    {
                        _itemsElementName = value;
                    }
                }
                else
                {
                    _itemsElementName = value;
                }
            }
        }

        [XmlText]
        public List<string> Text
        {
            get
            {
                if ((_text == null))
                {
                    _text = new List<string>();
                }
                return _text;
            }
            set
            {
                if ((_text != null))
                {
                    if ((_text.Equals(value) != true))
                    {
                        _text = value;
                    }
                }
                else
                {
                    _text = value;
                }
            }
        }
    }
}