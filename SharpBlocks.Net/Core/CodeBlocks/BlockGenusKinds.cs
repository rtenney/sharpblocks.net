﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpBlocks.Net.Core.CodeBlocks
{
    public enum BlockGenusKinds
    {
        data,
        command,
        function,
        procedure,
        param,
        variable
    }
}
