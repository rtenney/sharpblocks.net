// <copyright file="EntityBaseTTest.OnPropertyChanged.g.cs">Copyright �  2013</copyright>
// <auto-generated>
// This file contains automatically generated unit tests.
// Do NOT modify this file manually.
// 
// When Pex is invoked again,
// it might remove or update any previously generated unit tests.
// 
// If the contents of this file becomes outdated, e.g. if it does not
// compile anymore, you may delete this file and invoke Pex again.
// </auto-generated>
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Pex.Framework.Generated;

namespace SharpBlocks.Net.Core
{
    public partial class EntityBaseTTest
    {
[TestMethod]
[PexGeneratedBy(typeof(EntityBaseTTest))]
public void OnPropertyChanged26()
{
    EntityBase<int> entityBase;
    entityBase = new EntityBase<int>();
    this.OnPropertyChanged<int>(entityBase, (string)null);
    Assert.IsNotNull((object)entityBase);
}
    }
}
