﻿using SharpBlocks.Net.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using SharpBlocks.Net.Core.Workspace;

namespace SharpBlocks.Net.Tests
{
    
    
    /// <summary>
    ///This is a test class for PageTest and is intended
    ///to contain all PageTest Unit Tests
    ///</summary>
    [TestClass()]
    public class PageTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Page Constructor
        ///</summary>
        [TestMethod()]
        public void PageConstructorTest()
        {
            Page target = new Page();
            Assert.IsNotNull(target);
            //Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for PageColor
        ///</summary>
        [TestMethod()]
        public void PageColorTest()
        {
            Page target = new Page(); // TODO: Initialize to an appropriate value
            string expected = "255 255 255"; // TODO: Initialize to an appropriate value
            string actual;
            target.PageColor = expected;
            actual = target.PageColor;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for PageDrawer
        ///</summary>
        [TestMethod()]
        public void PageDrawerTest()
        {
            Page target = new Page(); // TODO: Initialize to an appropriate value
            string expected = "test"; // TODO: Initialize to an appropriate value
            string actual;
            target.PageDrawer = expected;
            actual = target.PageDrawer;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for PageDrawers
        ///</summary>
        [TestMethod()]
        public void PageDrawersTest()
        {
            Page target = new Page(); // TODO: Initialize to an appropriate value
            List<string> expected = new List<string>(); // TODO: Initialize to an appropriate value
            List<string> actual;
            target.PageDrawers = expected;
            actual = target.PageDrawers;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for PageName
        ///</summary>
        [TestMethod()]
        public void PageNameTest()
        {
            Page target = new Page(); // TODO: Initialize to an appropriate value
            string expected = "test"; // TODO: Initialize to an appropriate value
            string actual;
            target.PageName = expected;
            actual = target.PageName;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for PageShape
        ///</summary>
        [TestMethod()]
        public void PageShapeTest()
        {
            Page target = new Page(); // TODO: Initialize to an appropriate value
            string expected = "test"; // TODO: Initialize to an appropriate value
            string actual;
            target.PageShape = expected;
            actual = target.PageShape;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for PageWidth
        ///</summary>
        [TestMethod()]
        public void PageWidthTest()
        {
            Page target = new Page(); // TODO: Initialize to an appropriate value
            int expected = 15; // TODO: Initialize to an appropriate value
            int actual;
            target.PageWidth = expected;
            actual = target.PageWidth;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
