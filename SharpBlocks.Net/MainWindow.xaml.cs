﻿using System.IO;
using System.Windows;
using SharpBlocks.Net.Core;
using SharpBlocks.Net.ViewModel;
using System;

namespace SharpBlocks.Net
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            Closing += (s, e) => ViewModelLocator.Cleanup();
            //var definition = Definition();
            //File.WriteAllText(@"processed.xml", definition.Serialize());
        }

        //[STAThread]
        //private BlockLanguageDefinition Definition()
        //{
        //    return File.ReadAllText(@"lang_def.xml").Deserialize<BlockLanguageDefinition>();
        //}
    }
}