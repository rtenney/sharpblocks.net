﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace SharpBlocks.Net.Core.Workspace
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [DesignerCategory(@"code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class PageDrawer //: EntityBase<PageDrawer>
    {
        
        private List<string> _blockGenusMember;

        [XmlElement("BlockGenusMember", Order = 0)]
        public List<string> BlockGenusMember
        {
            get
            {
                if ((_blockGenusMember == null))
                {
                    _blockGenusMember = new List<string>();
                }
                return _blockGenusMember;
            }
            set
            {
                if ((_blockGenusMember != null))
                {
                    if ((_blockGenusMember.Equals(value) != true))
                    {
                        _blockGenusMember = value;
                        //OnPropertyChanged("BlockGenusMember");
                    }
                }
                else
                {
                    _blockGenusMember = value;
                    //OnPropertyChanged("BlockGenusMember");
                }
            }
        }
    }
}