﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Diagnostics;
using SharpBlocks.Net.Core.Utilities;

namespace SharpBlocks.Net.Core.CodeBlocks
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [DesignerCategory(@"code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    [DebuggerDisplay("{ConnectionBlockId}: Connector: {Label} Kind: {Kind}")]
    public class BlockConnector
    {
        private long _connectionBlockId = Block.NULL;
        private ConnectorKind _connectorKind;
        private string _connectorType;
        private DefaultArgument _defaultArguments;
        private string _label;
        private PositionType _positionType;

        #region Constructors

        public BlockConnector()
        {
            IsLabelEditable = false;
            _positionType = PositionType.single;
            IsExpandable = false;
        }

        public BlockConnector(BlockConnector.ConnectorKind kind, PositionType positionType, string label, bool isLabelEditable, bool isExpandable, string expandGroup, long connectionBlockId) :
            this(kind, positionType, label, isLabelEditable, isExpandable, connectionBlockId)
        {
            this.ExpandGroup = expandGroup == null ? "" : expandGroup;
        }

        public BlockConnector(BlockConnector blockConnector)
            : this(blockConnector.Kind, blockConnector.Position_Type, blockConnector.Label, blockConnector.IsLabelEditable, blockConnector.IsExpandable, blockConnector.ConnectionBlockId)
        {
            this.HasDefaultArguments = blockConnector.HasDefaultArguments;
            this.DefaultArgument = blockConnector.DefaultArgument;
            this.IsLabelEditable = blockConnector.IsLabelEditable;
            this.ExpandGroup = blockConnector.ExpandGroup;
        }

        public BlockConnector(BlockConnector.ConnectorKind kind, string label, long socketBlockID)
            : this(kind, PositionType.single, label, false, false, socketBlockID)
        {
        }

        public BlockConnector(string label, BlockConnector.ConnectorKind kind, bool isLabelEditable, bool isExpandable)
            : this(kind, PositionType.single, label, isLabelEditable, isExpandable, Block.NULL)
        {
        }

        public BlockConnector(BlockConnector.ConnectorKind kind, PositionType positionType, string label, bool isLabelEditable, bool isExpandable, long connectionBlockId)
        {
            this.Kind = kind;
            this.Position_Type = positionType;
            this.Label = label;
            this.IsLabelEditable = isLabelEditable;
            this.IsExpandable = isExpandable;
            this.ConnectionBlockId = connectionBlockId;
            this.InitialKind = kind;
        }

        #endregion Constructors

        #region Enumerations

        [GeneratedCode("System.Xml", "4.0.30319.18213")]
        [Serializable]
        [XmlType(AnonymousType = true)]
        public enum ConnectorKind
        {
            /// <remarks />
            plug,

            /// <remarks />
            socket,

            list
        }

        [GeneratedCode("System.Xml", "4.0.30319.18213")]
        [Serializable]
        [XmlType(AnonymousType = true)]
        public enum PositionType
        {
            /// <remarks />
            single,

            /// <remarks />
            mirror,

            /// <remarks />
            bottom,

            top
        }

        //[GeneratedCode("System.Xml", "4.0.30319.18213")]
        //[Serializable]
        //[XmlType(AnonymousType = true)]
        //public enum Type
        //{
        //    number,
        //    boolean,
        //    poly,
        //    @string,
        //    cmd,
        //    [XmlEnum("poly-list")]
        //    poly_list
        //}

        #endregion Enumerations

        #region Properties

        [XmlIgnore]
        public long ConnectionBlockId
        {
            get { return _connectionBlockId; }
            set { _connectionBlockId = value; }
        }

        [XmlAttribute("connector-type")]
        public string ConnectorType
        {
            get
            {
                return _connectorType;
            }
            set
            {
                if ((_connectorType != null))
                {
                    if ((_connectorType.Equals(value) != true))
                    {
                        _connectorType = value;
                    }
                }
                else
                {
                    _connectorType = value;
                }
            }
        }

        public bool ShouldSerializeDefaultArgument()
        {
            return !(string.IsNullOrEmpty(this.DefaultArgument.GenusName) && string.IsNullOrEmpty(this.DefaultArgument.Label));
        }

        [XmlElement("DefaultArg", Order = 0)]
        public DefaultArgument DefaultArgument
        {
            get
            {
                if ((_defaultArguments == null))
                {
                    _defaultArguments = new DefaultArgument();
                }
                return _defaultArguments;
            }
            set
            {
                if ((_defaultArguments != null))
                {
                    if ((_defaultArguments.Equals(value) != true))
                    {
                        _defaultArguments = value;
                    }
                }
                else
                {
                    _defaultArguments = value;
                }
            }
        }

        [XmlIgnore]
        public string ExpandGroup { get; set; }

        [XmlIgnore]
        public bool HasBlock
        {
            get
            {
                return !ConnectionBlockId.Equals(Block.NULL);
            }
        }

        [XmlIgnore]
        public bool HasDefaultArguments { get; set; }

        [XmlIgnore]
        public ConnectorKind InitialKind { get; set; }

        [XmlIgnore]
        public bool IsExpandable { get; set; }

        [XmlAttribute("is-expandable")]
        [DefaultValue("no")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string IsExpandableString
        {
            get
            {
                return XmlTypeConverter.ConvertFromBoolean(IsExpandable);
            }
            set
            {
                if ((IsExpandable.Equals(value) != true))
                {
                    IsExpandable = XmlTypeConverter.ConvertToBoolean(value);

                    //OnPropertyChanged("IsExpandableString");
                }
            }
        }

        [XmlIgnore]
        public bool IsLabelEditable { get; set; }

        [XmlAttribute("label-editable")]
        [DefaultValue("no")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string IsLabelEditableString
        {
            get
            {
                return XmlTypeConverter.ConvertFromBoolean(IsLabelEditable);
            }
            set
            {
                if ((IsLabelEditable.Equals(value) != true))
                {
                    IsLabelEditable = XmlTypeConverter.ConvertToBoolean(value);
                }
            }
        }

        [XmlAttribute("connector-kind")]
        public ConnectorKind Kind
        {
            get
            {
                return _connectorKind;
            }
            set
            {
                if ((_connectorKind.Equals(value) != true))
                {
                    _connectorKind = value;
                }
            }
        }

        [XmlAttribute("label")]
        public string Label
        {
            get
            {
                return _label;
            }
            set
            {
                if ((_label != null))
                {
                    if ((_label.Equals(value) != true))
                    {
                        _label = value;
                    }
                }
                else
                {
                    _label = value;
                }
            }
        }

        [XmlAttribute("position-type")]
        [DefaultValue(PositionType.single)]
        public PositionType Position_Type
        {
            get
            {
                return _positionType;
            }
            set
            {
                if ((_positionType.Equals(value) != true))
                {
                    _positionType = value;
                }
            }
        }

        #endregion Properties

        public static BlockConnector InstantiateFromState(object memento)
        {
            if (memento is BlockConnectorState)
            {
                BlockConnectorState state = (BlockConnectorState)memento;

                BlockConnector instance = new BlockConnector(state.Kind, state.Position_Type, state.Label, state.IsLabelEditable, state.IsExpandable, state.ConnectionBlockId);
                instance.IsLabelEditable = state.IsLabelEditable;

                if (state.HasDefaultArguments)
                {
                    instance.SetDefaultArgument(state.DefaultArgumentGenusName, state.DefaultArgumentLabel);
                }
                return instance;
            }
            return null;
        }

        public object GetState()
        {
            BlockConnectorState state = new BlockConnectorState();

            state.Kind = this.Kind;
            state.InitialKind = this.InitialKind;
            state.Position_Type = this.Position_Type;
            state.Label = this.Label;
            state.ConnectionBlockId = this.ConnectionBlockId;

            //Default Args stuff
            if (this.HasDefaultArguments)
            {
                state.DefaultArgumentGenusName = this.DefaultArgument.GenusName;
                state.DefaultArgumentLabel = this.DefaultArgument.Label;
                state.HasDefaultArguments = true;
            }
            else
            {
                state.DefaultArgumentGenusName = null;
                state.DefaultArgumentLabel = null;
                state.HasDefaultArguments = false;
            }
            state.IsExpandable = this.IsExpandable;
            state.IsLabelEditable = this.IsLabelEditable;
            state.ExpandGroup = this.ExpandGroup;

            return state;
        }

        public long LinkDefaultArgument()
        {
            //checks if connector has a def arg or if connector already has a block
            if (HasDefaultArguments && ConnectionBlockId == Block.NULL)
            {
                Block block = new Block(DefaultArgument.GenusName, DefaultArgument.Label);
                ConnectionBlockId = block.ConnectionBlockId;
                return ConnectionBlockId;
            }
            return Block.NULL;
        }

        public void LoadState(object memento)
        {
            if (memento is BlockConnectorState)
            {
                BlockConnectorState state = (BlockConnectorState)memento;

                this.Kind = (state.Kind);
                this.Position_Type = (state.Position_Type);
                this.Label = (state.Label);
                this.ConnectionBlockId = (state.ConnectionBlockId);

                if (state.HasDefaultArguments)
                {
                    this.SetDefaultArgument(state.DefaultArgumentGenusName, state.DefaultArgumentLabel);
                }
                else
                {
                    this.DefaultArgument = null;
                }

                this.IsExpandable = state.IsExpandable;
                this.IsLabelEditable = state.IsLabelEditable;
                this.ExpandGroup = state.ExpandGroup;
            }
        }

        public void SetDefaultArgument(string genusName, string label)
        {
            this.HasDefaultArguments = true;
            this.DefaultArgument = new DefaultArgument(genusName, label);
        }

        public override string ToString()
        {
            return String.Format("Connector label: {0}, Connector kind: {1}, blockID: {2} with pos type: {3}", Label, Kind, ConnectionBlockId, Position_Type);
        }

        private class BlockConnectorState
        {
            public long ConnectionBlockId = Block.NULL;

            public string DefaultArgumentGenusName { get; set; }

            public string DefaultArgumentLabel { get; set; }

            public string ExpandGroup { get; set; }

            //DefaultArg Stuff
            public bool HasDefaultArguments { get; set; }

            public BlockConnector.ConnectorKind InitialKind { get; set; }

            public bool IsExpandable { get; set; }

            public bool IsLabelEditable { get; set; }

            public BlockConnector.ConnectorKind Kind { get; set; }

            public string Label { get; set; }

            public PositionType Position_Type { get; set; }
        }
    }
}