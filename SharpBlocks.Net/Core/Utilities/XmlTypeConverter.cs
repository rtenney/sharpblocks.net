﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace SharpBlocks.Net.Core.Utilities
{
    public static class XmlTypeConverter
    {
        public static bool ConvertToBoolean(string value)
        {
            var result = false;

            if (value != null)
            {
                var newValue = value.ToLower();
                result = (newValue == "yes" || newValue == "1" || newValue == "true");
            }

            return result;
        }

        public static string ConvertFromBoolean(bool value)
        {
            return value ? "yes" : "no";
        }

        //public static int ConvertToInt(string value)
        //{
        //    var result = 0;

        //    if (value == null) return result;

        //    return int.TryParse(value, out result) ? result : 0;
        //}
    }
}
