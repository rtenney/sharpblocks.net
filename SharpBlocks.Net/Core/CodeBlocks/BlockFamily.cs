﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace SharpBlocks.Net.Core.CodeBlocks
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [DesignerCategory(@"code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class BlockFamily //: EntityBase<BlockFamily>
    {
        
        private List<string> _familyMember;

        [XmlElement("FamilyMember", Order = 0)]
        public List<string> FamilyMember
        {
            get
            {
                if ((_familyMember == null))
                {
                    _familyMember = new List<string>();
                }
                return _familyMember;
            }
            set
            {
                if ((_familyMember != null))
                {
                    if ((_familyMember.Equals(value) != true))
                    {
                        _familyMember = value;
                        //OnPropertyChanged("FamilyMember");
                    }
                }
                else
                {
                    _familyMember = value;
                    //OnPropertyChanged("FamilyMember");
                }
            }
        }
    }
}