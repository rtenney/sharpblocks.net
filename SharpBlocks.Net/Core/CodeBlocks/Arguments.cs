﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace SharpBlocks.Net.Core.CodeBlocks
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [DesignerCategory(@"code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(ElementName = "Arguments", Namespace = "", IsNullable = false)]
    public class Arguments
    {
        private string _n;

        private string _name;

        [XmlAttribute("n")]
        public string N
        {
            get { return _n; }
            set
            {
                if ((_n != null))
                {
                    if ((_n.Equals(value) != true))
                    {
                        _n = value;
                    }
                }
                else
                {
                    _n = value;
                }
            }
        }

        [XmlAttribute("name")]
        public string Name
        {
            get { return _name; }
            set
            {
                if ((_name != null))
                {
                    if ((_name.Equals(value) != true))
                    {
                        _name = value;
                    }
                }
                else
                {
                    _name = value;
                }
            }
        }
    }
}