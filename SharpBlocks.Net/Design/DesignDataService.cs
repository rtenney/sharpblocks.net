﻿using System;
using SharpBlocks.Net.Model;

namespace SharpBlocks.Net.Design
{
    public class DesignDataService : IDataService
    {
        public void GetData(Action<DataItem, Exception> callback)
        {
            // <pex>
            if (callback == (Action<DataItem, Exception>)null)
                throw new ArgumentNullException("callback");
            // </pex>
            // Use this to create design time data

            var item = new DataItem("Welcome to MVVM Light [design]");
            callback(item, null);
        }
    }
}