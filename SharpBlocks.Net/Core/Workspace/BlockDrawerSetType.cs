﻿using System;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

namespace SharpBlocks.Net.Core.Workspace
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [XmlType(AnonymousType = true)]
    public enum BlockDrawerSetType
    {
        /// <remarks />
        bar,

        /// <remarks />
        stack,
    }
}