﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;
using SharpBlocks.Net.Core.Utilities;

namespace SharpBlocks.Net.Core.Workspace
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [DesignerCategory(@"code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class MiniMap //: EntityBase<MiniMap>
    {

        public bool IsEnabled { get; set; }

        public MiniMap()
        {
            IsEnabled = true;
        }

        [XmlAttribute("enabled")]
        [DefaultValue("yes")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string EnabledString
        {
            get { return XmlTypeConverter.ConvertFromBoolean(IsEnabled); }
            set
            {
                if ((IsEnabled.Equals(value) != true))
                {
                    IsEnabled = XmlTypeConverter.ConvertToBoolean(value);
                    //OnPropertyChanged("EnabledString");
                }
            }
        }
    }
}