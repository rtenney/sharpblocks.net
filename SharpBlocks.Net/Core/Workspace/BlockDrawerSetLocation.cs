﻿using System;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

namespace SharpBlocks.Net.Core.Workspace
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [XmlType(AnonymousType = true)]
    public enum BlockDrawerSetLocation
    {
        /// <remarks />
        east,

        /// <remarks />
        west,

        /// <remarks />
        north,

        /// <remarks />
        south,

        /// <remarks />
        northeast,

        /// <remarks />
        southeast,

        /// <remarks />
        southwest,

        /// <remarks />
        northwest,
    }
}