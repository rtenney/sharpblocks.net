using System;
using System.ComponentModel;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SharpBlocks.Net.Core
{
    public class EntityBase<T> : INotifyPropertyChanged
    {
        //private static XmlSerializer _serializer;

        //private static XmlSerializer Serializer
        //{
        //    get
        //    {
        //        if ((_serializer == null))
        //        {
        //            _serializer = new XmlSerializer(typeof(T));
        //        }
        //        return _serializer;
        //    }
        //}

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        public virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if ((handler != null))
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        //#region Serialize/Deserialize

        ///// <summary>
        /////   Serializes current EntityBase object into an XML document
        ///// </summary>
        ///// <returns>string XML value</returns>
        //public virtual string Serialize()
        //{
        //    StreamReader streamReader = null;
        //    MemoryStream memoryStream = null;
        //    try
        //    {
        //        memoryStream = new MemoryStream();
        //        Serializer.Serialize(memoryStream, this);
        //        memoryStream.Seek(0, SeekOrigin.Begin);
        //        streamReader = new StreamReader(memoryStream);
        //        return streamReader.ReadToEnd();
        //    }
        //    finally
        //    {
        //        if ((streamReader != null))
        //        {
        //            streamReader.Dispose();
        //        }
        //        if ((memoryStream != null))
        //        {
        //            memoryStream.Dispose();
        //        }
        //    }
        //}

        ///// <summary>
        /////   Deserializes workflow markup into an EntityBase object
        ///// </summary>
        ///// <param name = "xml">string workflow markup to deserialize</param>
        ///// <param name = "obj">Output EntityBase object</param>
        ///// <param name = "exception">output Exception value if deserialize failed</param>
        ///// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        //public static bool Deserialize(string xml, out T obj, out Exception exception)
        //{
        //    exception = null;
        //    obj = default(T);
        //    try
        //    {
        //        obj = Deserialize(xml);
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        exception = ex;
        //        return false;
        //    }
        //}

        //public static bool Deserialize(string xml, out T obj)
        //{
        //    Exception exception = null;
        //    return Deserialize(xml, out obj, out exception);
        //}

        //public static T Deserialize(string xml)
        //{
        //    StringReader stringReader = null;
        //    try
        //    {
        //        stringReader = new StringReader(xml);
        //        return ((T)(Serializer.Deserialize(XmlReader.Create(stringReader))));
        //    }
        //    finally
        //    {
        //        if ((stringReader != null))
        //        {
        //            stringReader.Dispose();
        //        }
        //    }
        //}

        ///// <summary>
        /////   Serializes current EntityBase object into file
        ///// </summary>
        ///// <param name = "fileName">full path of outupt xml file</param>
        ///// <param name = "exception">output Exception value if failed</param>
        ///// <returns>true if can serialize and save into file; otherwise, false</returns>
        //public virtual bool SaveToFile(string fileName, out Exception exception)
        //{
        //    exception = null;
        //    try
        //    {
        //        SaveToFile(fileName);
        //        return true;
        //    }
        //    catch (Exception e)
        //    {
        //        exception = e;
        //        return false;
        //    }
        //}

        //public virtual void SaveToFile(string fileName)
        //{
        //    StreamWriter streamWriter = null;
        //    try
        //    {
        //        var xmlString = Serialize();
        //        var xmlFile = new FileInfo(fileName);
        //        streamWriter = xmlFile.CreateText();
        //        streamWriter.WriteLine(xmlString);
        //        streamWriter.Close();
        //    }
        //    finally
        //    {
        //        if ((streamWriter != null))
        //        {
        //            streamWriter.Dispose();
        //        }
        //    }
        //}

        ///// <summary>
        /////   Deserializes xml markup from file into an EntityBase object
        ///// </summary>
        ///// <param name = "fileName">string xml file to load and deserialize</param>
        ///// <param name = "obj">Output EntityBase object</param>
        ///// <param name = "exception">output Exception value if deserialize failed</param>
        ///// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        //public static bool LoadFromFile(string fileName, out T obj, out Exception exception)
        //{
        //    exception = null;
        //    obj = default(T);
        //    try
        //    {
        //        obj = LoadFromFile(fileName);
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        exception = ex;
        //        return false;
        //    }
        //}

        //public static bool LoadFromFile(string fileName, out T obj)
        //{
        //    Exception exception = null;
        //    return LoadFromFile(fileName, out obj, out exception);
        //}

        //public static T LoadFromFile(string fileName)
        //{
        //    FileStream file = null;
        //    StreamReader sr = null;
        //    try
        //    {
        //        file = new FileStream(fileName, FileMode.Open, FileAccess.Read);
        //        sr = new StreamReader(file);
        //        var xmlString = sr.ReadToEnd();
        //        sr.Close();
        //        file.Close();
        //        return Deserialize(xmlString);
        //    }
        //    finally
        //    {
        //        if ((file != null))
        //        {
        //            file.Dispose();
        //        }
        //        if ((sr != null))
        //        {
        //            sr.Dispose();
        //        }
        //    }
        //}

        //#endregion

        #region Clone method

        /// <summary>
        ///   Create a clone of this T object
        /// </summary>
        public virtual T Clone()
        {
            var clone = MemberwiseClone();
            return (T)clone;
        }

        #endregion
    }
}