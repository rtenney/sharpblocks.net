﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace SharpBlocks.Net.Core.CodeBlocks
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [DesignerCategory(@"code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(ElementName = "note", Namespace = "", IsNullable = false)]
    public class Note //: EntityBase<Note>
    {
        private List<object> _items;

        private List<string> _text;

        [XmlElement("Arguments", typeof (Arguments), Order = 0)]
        [XmlElement("i", typeof (string), Order = 0)]
        public List<object> Items
        {
            get
            {
                if ((_items == null))
                {
                    _items = new List<object>();
                }
                return _items;
            }
            set
            {
                if ((_items != null))
                {
                    if ((_items.Equals(value) != true))
                    {
                        _items = value;
                        //OnPropertyChanged("Items");
                    }
                }
                else
                {
                    _items = value;
                    //OnPropertyChanged("Items");
                }
            }
        }

        [XmlText]
        public List<string> Text
        {
            get
            {
                if ((_text == null))
                {
                    _text = new List<string>();
                }
                return _text;
            }
            set
            {
                if ((_text != null))
                {
                    if ((_text.Equals(value) != true))
                    {
                        _text = value;
                        //OnPropertyChanged("Text");
                    }
                }
                else
                {
                    _text = value;
                    //OnPropertyChanged("Text");
                }
            }
        }
    }
}