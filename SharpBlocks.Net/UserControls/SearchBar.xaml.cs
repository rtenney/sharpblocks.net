﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SharpBlocks.Net.UserControls
{
    /// <summary>
    /// Interaction logic for SearchBar.xaml
    /// </summary>
    public partial class SearchBar : UserControl
    {
        public SearchBar()
        {
            InitializeComponent();
        }

        /// <summary>
        /// The <see cref="DefaultControl" /> dependency property's name.
        /// </summary>
        public const string DefaultControlPropertyName = "DefaultControl";

        /// <summary>
        /// Gets or sets the value of the <see cref="DefaultControl" />
        /// property. This is a dependency property.
        /// </summary>
        public Control DefaultControl
        {
            get
            {
                return (Control)GetValue(DefaultControlProperty);
            }
            set
            {
                SetValue(DefaultControlProperty, value);
            }
        }

        /// <summary>
        /// Identifies the <see cref="DefaultControl" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty DefaultControlProperty = DependencyProperty.Register(
            DefaultControlPropertyName,
            typeof(Control),
            typeof(SearchBar),
            new UIPropertyMetadata(default(Control)));

        private void QueryField_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = false;
            if (DefaultControl != null)
                if (e.Key == Key.Escape)
                {
                    DefaultControl.Focus();
                    e.Handled = true;
                }
        }

        /// <summary>
        /// The <see cref="SearchText" /> dependency property's name.
        /// </summary>
        public const string SearchTextPropertyName = "SearchText";

        /// <summary>
        /// Gets or sets the value of the <see cref="SearchText" />
        /// property. This is a dependency property.
        /// </summary>
        public string SearchText
        {
            get
            {
                return (string)GetValue(SearchTextProperty);
            }
            set
            {
                SetValue(SearchTextProperty, value);
            }
        }

        /// <summary>
        /// Identifies the <see cref="SearchText" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty SearchTextProperty = DependencyProperty.Register(
            SearchTextPropertyName,
            typeof(string),
            typeof(SearchBar),
            new UIPropertyMetadata(string.Empty));
    }
}
