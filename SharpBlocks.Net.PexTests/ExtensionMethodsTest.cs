// <copyright file="ExtensionMethodsTest.cs">Copyright �  2013</copyright>

using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SharpBlocks.Net;

namespace SharpBlocks.Net
{
    [TestClass]
    [PexClass(typeof(ExtensionMethods))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    public partial class ExtensionMethodsTest
    {
        [PexGenericArguments(typeof(int))]
        [PexMethod(MaxConditions = 1000)]
        public T Deserialize<T>(string serializedData)
        {
            T result = ExtensionMethods.Deserialize<T>(serializedData);
            return result;
            // TODO: add assertions to method ExtensionMethodsTest.Deserialize(String)
        }
    }
}
