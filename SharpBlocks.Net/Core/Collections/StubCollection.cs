﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpBlocks.Net.Core.CodeBlocks;

namespace SharpBlocks.Net.Core.Collections
{
    public class StubCollection: List<Stub>
    {
        private BlockGenus _parentGenus;

        public StubCollection():
            base(new List<Stub>())
        {

        }

        public StubCollection(BlockGenus parentGenus)
            : this()
        {
            this._parentGenus = parentGenus;
        }

        public new void Add(Stub item)
        {
            base.Add(item);
            BlockGenus newStubGenus = new BlockGenus(item.StubGenus.ToString(), item.StubGenus.ToString() + _parentGenus.GenusName);

            BlockGenus.NameToGenus.Add(newStubGenus.GenusName, newStubGenus);
            _parentGenus.StubList.Add(newStubGenus.GenusName);
        }
    }
}
