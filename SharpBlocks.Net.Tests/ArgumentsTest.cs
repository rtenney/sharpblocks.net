﻿using SharpBlocks.Net.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SharpBlocks.Net.Core.CodeBlocks;

namespace SharpBlocks.Net.Tests
{
    
    
    /// <summary>
    ///This is a test class for ArgumentsTest and is intended
    ///to contain all ArgumentsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ArgumentsTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Arguments Constructor
        ///</summary>
        [TestMethod()]
        public void ArgumentsConstructorTest()
        {
            Arguments target = new Arguments();
            Assert.IsNotNull(target);
            //Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for N
        ///</summary>
        [TestMethod()]
        public void NTest()
        {
            Arguments target = new Arguments(); // TODO: Initialize to an appropriate value
            string expected = "test"; // TODO: Initialize to an appropriate value
            string actual;
            target.N = expected;
            actual = target.N;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GennusName
        ///</summary>
        [TestMethod()]
        public void NameTest()
        {
            Arguments target = new Arguments(); // TODO: Initialize to an appropriate value
            string expected = "test"; // TODO: Initialize to an appropriate value
            string actual;
            target.Name = expected;
            actual = target.Name;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
