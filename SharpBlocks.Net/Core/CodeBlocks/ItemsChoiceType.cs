﻿using System;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

namespace SharpBlocks.Net.Core.CodeBlocks
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [XmlType(IncludeInSchema = false)]
    public enum ItemsChoiceType
    {
        /// <remarks />
        arg,

        /// <remarks />
        br,

        /// <remarks />
        em,

        /// <remarks />
        i,

        /// <remarks />
        note,
    }
}