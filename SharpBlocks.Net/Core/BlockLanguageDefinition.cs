﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using SharpBlocks.Net.Core.CodeBlocks;
using SharpBlocks.Net.Core.Workspace;
using SharpBlocks.Net.Core.Collections;

namespace SharpBlocks.Net.Core
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [DesignerCategory(@"code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(ElementName = "BlockLangDef", Namespace = "", IsNullable = false)]
    public class BlockLanguageDefinition
    {
        private List<BlockConnectorShape> _blockConnectorShapesField;

        private List<BlockDrawerSet> _blockDrawerSetsField;
        private List<BlockFamily> _blockFamiliesField;
        private BlockGenusCollection _blockGenusesField;
        private MiniMap _miniMapField;

        private Pages _pagesField;

        private TrashCan _trashCanField;

        [XmlArray(Order = 0)]
        [XmlArrayItem("BlockConnectorShape", IsNullable = false)]
        public List<BlockConnectorShape> BlockConnectorShapes
        {
            get
            {
                if ((_blockConnectorShapesField == null))
                {
                    _blockConnectorShapesField = new List<BlockConnectorShape>();
                }
                return _blockConnectorShapesField;
            }
            set
            {
                if ((_blockConnectorShapesField != null))
                {
                    if ((_blockConnectorShapesField.Equals(value) != true))
                    {
                        _blockConnectorShapesField = value;
                    }
                }
                else
                {
                    _blockConnectorShapesField = value;
                }
            }
        }

        [XmlArray(Order = 3)]
        [XmlArrayItem("BlockDrawerSet", IsNullable = false)]
        public List<BlockDrawerSet> BlockDrawerSets
        {
            get
            {
                if ((_blockDrawerSetsField == null))
                {
                    _blockDrawerSetsField = new List<BlockDrawerSet>();
                }
                return _blockDrawerSetsField;
            }
            set
            {
                if ((_blockDrawerSetsField != null))
                {
                    if ((_blockDrawerSetsField.Equals(value) != true))
                    {
                        _blockDrawerSetsField = value;
                    }
                }
                else
                {
                    _blockDrawerSetsField = value;
                }
            }
        }

        [XmlArray(Order = 2)]
        [XmlArrayItem("BlockFamily", IsNullable = false)]
        [XmlArrayItem("FamilyMember", IsNullable = false, NestingLevel = 1)]
        public List<BlockFamily> BlockFamilies
        {
            get
            {
                if ((_blockFamiliesField == null))
                {
                    _blockFamiliesField = new List<BlockFamily>();
                }
                return _blockFamiliesField;
            }
            set
            {
                if ((_blockFamiliesField != null))
                {
                    if ((_blockFamiliesField.Equals(value) != true))
                    {
                        _blockFamiliesField = value;
                    }
                }
                else
                {
                    _blockFamiliesField = value;
                }
            }
        }

        [XmlArray(Order = 1)]
        [XmlArrayItem("BlockGenus", IsNullable = false)]
        public BlockGenusCollection BlockGenuses
        {
            get
            {
                if ((_blockGenusesField == null))
                {
                    _blockGenusesField = new BlockGenusCollection();
                }
                return _blockGenusesField;
            }
            set
            {
                if ((_blockGenusesField != null))
                {
                    if ((_blockGenusesField.Equals(value) != true))
                    {
                        _blockGenusesField = value;
                    }
                }
                else
                {
                    _blockGenusesField = value;
                }
            }
        }

        [XmlElement(Order = 6)]
        public MiniMap MiniMap
        {
            get
            {
                if ((_miniMapField == null))
                {
                    _miniMapField = new MiniMap();
                }
                return _miniMapField;
            }
            set
            {
                if ((_miniMapField != null))
                {
                    if ((_miniMapField.Equals(value) != true))
                    {
                        _miniMapField = value;
                    }
                }
                else
                {
                    _miniMapField = value;
                }
            }
        }

        [XmlElement(Order = 4)]
        public Pages Pages
        {
            get
            {
                if ((_pagesField == null))
                {
                    _pagesField = new Pages();
                }
                return _pagesField;
            }
            set
            {
                if ((_pagesField != null))
                {
                    if ((_pagesField.Equals(value) != true))
                    {
                        _pagesField = value;
                    }
                }
                else
                {
                    _pagesField = value;
                }
            }
        }

        [XmlElement(Order = 5)]
        public TrashCan TrashCan
        {
            get
            {
                if ((_trashCanField == null))
                {
                    _trashCanField = new TrashCan();
                }
                return _trashCanField;
            }
            set
            {
                if ((_trashCanField != null))
                {
                    if ((_trashCanField.Equals(value) != true))
                    {
                        _trashCanField = value;
                    }
                }
                else
                {
                    _trashCanField = value;
                }
            }
        }
    }
}