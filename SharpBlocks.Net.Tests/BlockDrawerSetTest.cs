﻿using SharpBlocks.Net.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using SharpBlocks.Net.Core.Workspace;

namespace SharpBlocks.Net.Tests
{
    
    
    /// <summary>
    ///This is a test class for BlockDrawerSetTest and is intended
    ///to contain all BlockDrawerSetTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BlockDrawerSetTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for BlockDrawerSet Constructor
        ///</summary>
        [TestMethod()]
        public void BlockDrawerSetConstructorTest()
        {
            BlockDrawerSet target = new BlockDrawerSet();
            Assert.IsNotNull(target);
            //Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for BlockDrawer
        ///</summary>
        [TestMethod()]
        public void BlockDrawerTest()
        {
            BlockDrawerSet target = new BlockDrawerSet(); // TODO: Initialize to an appropriate value
            List<BlockDrawer> expected = new List<BlockDrawer>(); // TODO: Initialize to an appropriate value
            List<BlockDrawer> actual;
            target.BlockDrawer = expected;
            actual = target.BlockDrawer;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for HasWindowPerDrawer
        ///</summary>
        [TestMethod()]
        public void HasWindowPerDrawerTest()
        {
            BlockDrawerSet target = new BlockDrawerSet(); // TODO: Initialize to an appropriate value
            string expected = "yes"; // TODO: Initialize to an appropriate value
            string actual;
            target.HasWindowPerDrawerString = expected;
            actual = target.HasWindowPerDrawerString;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsDrawerDraggable
        ///</summary>
        [TestMethod()]
        public void IsDrawerDraggableTest()
        {
            BlockDrawerSet target = new BlockDrawerSet(); // TODO: Initialize to an appropriate value
            string expected = "yes"; // TODO: Initialize to an appropriate value
            string actual;
            target.IsDrawerDraggableString = expected;
            actual = target.IsDrawerDraggableString;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Location
        ///</summary>
        [TestMethod()]
        public void LocationTest()
        {
            BlockDrawerSet target = new BlockDrawerSet(); // TODO: Initialize to an appropriate value
            BlockDrawerSetLocation expected = BlockDrawerSetLocation.east;
            BlockDrawerSetLocation actual;
            target.Location = expected;
            actual = target.Location;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GennusName
        ///</summary>
        [TestMethod()]
        public void NameTest()
        {
            BlockDrawerSet target = new BlockDrawerSet(); // TODO: Initialize to an appropriate value
            string expected = "Test"; // TODO: Initialize to an appropriate value
            string actual;
            target.Name = expected;
            actual = target.Name;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Type
        ///</summary>
        [TestMethod()]
        public void TypeTest()
        {
            BlockDrawerSet target = new BlockDrawerSet(); // TODO: Initialize to an appropriate value
            BlockDrawerSetType expected = BlockDrawerSetType.bar;
            BlockDrawerSetType actual;
            target.Type = expected;
            actual = target.Type;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Width
        ///</summary>
        [TestMethod()]
        public void WidthTest()
        {
            BlockDrawerSet target = new BlockDrawerSet(); // TODO: Initialize to an appropriate value
            int expected = 15; // TODO: Initialize to an appropriate value
            int actual;
            target.Width = expected;
            actual = target.Width;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
