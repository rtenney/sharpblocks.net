﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace SharpBlocks.Net.Core.CodeBlocks
{
    [GeneratedCode("System.Xml", "4.0.30319.18213")]
    [Serializable]
    [DesignerCategory(@"code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class LangSpecProperty //: EntityBase<LangSpecProperty>
    {
        private string _key;

        private string _value;

        [XmlAttribute("key")]
        public string Key
        {
            get { return _key; }
            set
            {
                if ((_key != null))
                {
                    if ((_key.Equals(value) != true))
                    {
                        _key = value;
                        //OnPropertyChanged("Key");
                    }
                }
                else
                {
                    _key = value;
                    //OnPropertyChanged("Key");
                }
            }
        }

        [XmlAttribute("value")]
        public string Value
        {
            get { return _value; }
            set
            {
                if ((value != null))
                {
                    if ((value.Equals(_value) != true))
                    {
                        _value = value;
                        //OnPropertyChanged("Value");
                    }
                }
                else
                {
                    _value = value;
                    //OnPropertyChanged("Value");
                }
            }
        }
    }
}