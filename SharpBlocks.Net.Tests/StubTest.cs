﻿using SharpBlocks.Net.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using SharpBlocks.Net.Core.CodeBlocks;

namespace SharpBlocks.Net.Tests
{
    
    
    /// <summary>
    ///This is a test class for StubTest and is intended
    ///to contain all StubTest Unit Tests
    ///</summary>
    [TestClass()]
    public class StubTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Stub Constructor
        ///</summary>
        [TestMethod()]
        public void StubConstructorTest()
        {
            Stub target = new Stub();
            Assert.IsNotNull(target);
            //Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for LangSpecProperties
        ///</summary>
        [TestMethod()]
        public void LangSpecPropertiesTest()
        {
            Stub target = new Stub(); // TODO: Initialize to an appropriate value
            List<LangSpecProperty> expected = new List<LangSpecProperty>(); // TODO: Initialize to an appropriate value
            List<LangSpecProperty> actual;
            target.LangSpecProperties = expected;
            actual = target.LangSpecProperties;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Scope
        ///</summary>
        [TestMethod()]
        public void ScopeTest()
        {
            Stub target = new Stub(); // TODO: Initialize to an appropriate value
            string expected = "test"; // TODO: Initialize to an appropriate value
            string actual;
            target.Scope = expected;
            actual = target.Scope;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for StubGenus
        ///</summary>
        [TestMethod()]
        public void StubGenusTest()
        {
            Stub target = new Stub(); // TODO: Initialize to an appropriate value
            Stub.StubGenuses expected = Stub.StubGenuses.agent; // TODO: Initialize to an appropriate value
            Stub.StubGenuses actual;
            target.StubGenus = expected;
            actual = target.StubGenus;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
